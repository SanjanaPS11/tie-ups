import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { StorageService } from '../service/storage.service';
import { Globalservice } from '../service/globals.service';
import { AppSettings } from '../constants/constants.module';

@Injectable()
export class CommonRouteGuard implements CanActivate {

  constructor(private router:Router, private storageService: StorageService, private globalservice: Globalservice) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    
    //Return to mmg if there is no user logged In. If the user is logged in route to that url
    if(this.storageService.getItem('user') == null){
      this.router.navigate(['/'], { queryParams: { returnUrl: state.url }});
      return false;
    }
    else if(this.storageService.getItem('user').data.roleId == AppSettings.DRIVER_ROLE_ID || this.storageService.getItem('user').data.roleId == AppSettings.FRANCHISE_ROLE_ID){
      this.globalservice.changeScreen(this.storageService.getItem('user').data.roleId);
      return true;
    }
    else{
      this.router.navigate([this.storageService.getItem('user').data.path], { queryParams: { returnUrl: state.url }});
      return false;
    }
  }
}