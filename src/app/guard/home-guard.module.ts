import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { StorageService } from '../service/storage.service';
import { Globalservice } from '../service/globals.service';

@Injectable()
export class HomeRouteGuard implements CanActivate {

  constructor(private router:Router, private storageService: StorageService, private globalservice: Globalservice) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    
    //Return to mmg if there is no user logged In. If the user is logged in route to that url
    if(this.storageService.getItem('user') == null){
      this.globalservice.changeScreen(null);
      return true;
    }
    else{
      this.router.navigate([this.storageService.getItem('user').data.path], { queryParams: { returnUrl: state.url }});
      return false;
    }
  }
}