import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeRouteGuard } from './guard/home-guard.module';
import { DriverRouteGuard } from './guard/driver-guard.module';
import { FranchiseRouteGuard } from './guard/franchise-guard.module';
import { CommonRouteGuard } from './guard/common-guard.module';

import { MmgComponent } from '../app/home/mmg/mmg.component';
import { PrivacyPolicyComponent } from '../app/home/privacy-policy/privacy-policy.component';
import { DisclaimerComponent } from '../app/home/disclaimer/disclaimer.component';
import { FaqsComponent } from '../app/home/faqs/faqs.component';
import { DriverDetailComponent } from '../app/home/driver-detail/driver-detail.component';
import { FranchiseDetailComponent } from '../app/home/franchise-detail/franchise-detail.component';
import { ForgotPasswordComponent } from '../app/home/forgot-password/forgot-password.component';
import { ValidateComponent } from '../app/home/validate/validate.component';


import { OperationDashboardComponent } from './operation/operation-dashboard/operation-dashboard.component';
import { TieupsDashboardComponent } from './tie-ups/tieups-dashboard/tieups-dashboard.component';
import { OperationGuard } from './guard/opearation-gaurd.module';
import { TieupsGuard } from './guard/tieups-gaurd.module';
import { OperationFranchiseComponent } from './operation/operation-franchise/operation-franchise.component';
import { OperationFleetComponent } from './operation/operation-fleet/operation-fleet.component';
import { OperationWarehouseComponent } from './operation/operation-warehouse/operation-warehouse.component';
import { OperationCustomerComponent } from './operation/operation-customer/operation-customer.component';
import { OperationFranchisedashboardComponent } from './operation/operation-franchisedashboard/operation-franchisedashboard.component';
import { OperationEnquirydashboardComponent } from './operation/operation-enquirydashboard/operation-enquirydashboard.component';
import { OperationWarehousedashboardComponent } from './operation/operation-warehousedashboard/operation-warehousedashboard.component';
import { OperationFleetdashboardComponent } from './operation/operation-fleetdashboard/operation-fleetdashboard.component';
import { OperationFranchiseenquiryComponent } from './operation/operation-onboarding/operation-franchiseenquiry/operation-franchiseenquiry.component';
import { OperationFleetenquiryComponent } from './operation/operation-onboarding/operation-fleetenquiry/operation-fleetenquiry.component';
import { OperationVehiclerequestComponent } from './operation/operation-onboarding/operation-vehiclerequest/operation-vehiclerequest.component';
import { OperationDriverrequestComponent } from './operation/operation-onboarding/operation-driverrequest/operation-driverrequest.component';
import { OperationLabourrequestComponent } from './operation/operation-onboarding/operation-labourrequest/operation-labourrequest.component';
import { OperationEnterpriserequestComponent } from './operation/operation-onboarding/operation-enterpriserequest/operation-enterpriserequest.component';
import { OperationBookingComponent } from './operation/operation-booking/operation-booking.component';
import { OperationBiddingComponent } from './operation/operation-bidding/operation-bidding.component';
import { OperationWarehouseenquiryComponent } from './operation/operation-onboarding/operation-warehouseenquiry/operation-warehouseenquiry.component';
import { OperationFranchiseViewComponent } from './operation/operation-onboarding/operation-franchiseenquiry/operation-franchise-view/operation-franchise-view.component';
import { OperationDriveviewComponent } from './operation/operation-onboarding/operation-driverrequest/operation-driveview/operation-driveview.component';
import { OperationVehicleviewComponent } from './operation/operation-onboarding/operation-vehiclerequest/operation-vehicleview/operation-vehicleview.component';
import { OperationLabourviewComponent } from './operation/operation-onboarding/operation-labourrequest/operation-labourview/operation-labourview.component';
import { OperationFranchiseviewComponent } from './operation/operation-franchise/operation-franchiseview/operation-franchiseview.component';
import { OperationOfflinebookingComponent } from './operation/operation-booking/operation-offlinebooking/operation-offlinebooking.component';
import { OperationFranchisebookingsComponent } from './operation/operation-booking/operation-franchisebookings/operation-franchisebookings.component';
import { CompanyComponent} from './home/company/company.component';
import { CreateUpdateCompanyComponent } from './home/company/create-update-company/create-update-company.component';
import { ActiveDeactivateCompanyComponent } from './home/company/active-deactivate-company/active-deactivate-company.component';
import { PopupComponent } from './home/company/popup/popup.component';
import { UploadComponent } from './home/company/upload/upload.component';
import { MessageComponent } from './home/company/message/message.component';
import { LoginCompanyComponent } from './home/login-company/login-company.component';
import {AuthGuard} from './home/company/auth.guard';
const routes: Routes = [
  {path:'logincompany', component:LoginCompanyComponent },
  {path: 'message',component:MessageComponent},
  {path: 'upload',component:UploadComponent},
  {path: 'activeDeactiv',component:ActiveDeactivateCompanyComponent},
  {path: 'popup',component:PopupComponent},
  { path: 'companyCreateUpdate',component:CreateUpdateCompanyComponent},
  { path: 'company',canActivate:[AuthGuard],component:CompanyComponent},
  { path: 'mmg', component: MmgComponent, canActivate: [HomeRouteGuard] },
  { path: 'policy', component: PrivacyPolicyComponent, canActivate: [HomeRouteGuard] },
  { path: 'disclaimer', component: DisclaimerComponent,canActivate: [HomeRouteGuard] },
  { path: 'faqs', component: FaqsComponent, canActivate: [HomeRouteGuard] },
  { path: 'vehicle', component: DriverDetailComponent, canActivate: [HomeRouteGuard] },
  { path: 'franchise', component: FranchiseDetailComponent, canActivate: [HomeRouteGuard] },
  { path: 'forgotPassword', component: ForgotPasswordComponent, canActivate: [HomeRouteGuard] },
  { path: 'validate', component: ValidateComponent,canActivate: [HomeRouteGuard] },

 
  { path: 'operationalTeamDashboard', component: OperationDashboardComponent, canDeactivate: [OperationGuard] },
  {path: 'operationalFranchise', component: OperationFranchiseComponent, canDeactivate: [OperationGuard] },
  {path: 'operationFleet', component: OperationFleetComponent, canDeactivate: [OperationGuard] },
  {path: 'operationWarehouse', component: OperationWarehouseComponent, canDeactivate: [OperationGuard] },
  {path: 'operationCustomer', component: OperationCustomerComponent, canDeactivate: [OperationGuard] },
  {path: 'operationFracnhisedashboard', component: OperationFranchisedashboardComponent, canDeactivate: [OperationGuard] },
  {path: 'operationFleetdashboard', component: OperationFleetdashboardComponent,canDeactivate: [OperationGuard] },
  {path: 'operationWarehousedashboard', component: OperationWarehousedashboardComponent, canDeactivate: [OperationGuard] },
  {path: 'operationEnquirydashboard', component: OperationEnquirydashboardComponent, canDeactivate: [OperationGuard] },
  {path: 'operationFranchiseenquiry', component: OperationFranchiseenquiryComponent, canDeactivate: [OperationGuard] },
  {path: 'operationFleetenquiry', component: OperationFleetenquiryComponent, canDeactivate: [OperationGuard] },
  {path: 'operationWarehouseenquiry', component: OperationWarehouseenquiryComponent, canDeactivate: [OperationGuard] },
  {path: 'operationDriverrequest', component: OperationDriverrequestComponent, canDeactivate: [OperationGuard] },
  {path: 'operationVehiclerequest', component: OperationVehiclerequestComponent, canDeactivate: [OperationGuard] },
  {path: 'operationLabourrequest', component: OperationLabourrequestComponent, canDeactivate: [OperationGuard] },
  {path: 'operationEnterpriserequest', component: OperationEnterpriserequestComponent, canDeactivate: [OperationGuard] },
  {path: 'operationBooking', component: OperationBookingComponent, canDeactivate: [OperationGuard] },
  {path: 'operationBidding', component: OperationBiddingComponent, canDeactivate: [OperationGuard] },
  {path: 'franchiseView', component: OperationFranchiseViewComponent, canDeactivate: [OperationGuard] },
  {path: 'driverView', component: OperationDriveviewComponent, canDeactivate: [OperationGuard] },
  {path: 'vehicleView', component: OperationVehicleviewComponent, canDeactivate: [OperationGuard] },
  {path: 'labourView', component: OperationLabourviewComponent, canDeactivate: [OperationGuard] },
  {path: 'franchiseactiveview', component: OperationFranchiseviewComponent, canDeactivate: [OperationGuard] },
  {path: 'franchiseBooking', component: OperationFranchisebookingsComponent,canDeactivate: [OperationGuard] },
  {path: 'offlineBooking', component: OperationOfflinebookingComponent, canDeactivate: [OperationGuard] },

  
  
  
  
  
  
  

 
  
  { path: 'tieupsDashboard', component: TieupsDashboardComponent, canActivate: [TieupsGuard] },
  { path: '', component: MmgComponent, canActivate: [HomeRouteGuard] },
  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'top' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
