import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable()
export class Globalservice {
 /*do like franchisre */
  getLabourId(element: any) {
    throw new Error("Method not implemented.");
  }
  getVehicleId: any;
  getVehicle(element: any) {
    throw new Error("Method not implemented.");
  }


  /* Observable navItem source */
  private _navItemSource = new BehaviorSubject<number>(0);

  /* Observable OtpInfo */
  private _otpInfo = new BehaviorSubject<any>(null);

  /* Observable OtpInfo */
  private _source = new BehaviorSubject<any>(null);
  private _goodsType = new BehaviorSubject<any>(null);
  private _weightList = new BehaviorSubject<any>(null);

  /* Observable OtpInfo */
  private _destination = new BehaviorSubject<any>(null);

  private _franchise = new BehaviorSubject<any>(null);



  /* Observable driverId */
  private _driverId = new BehaviorSubject<any>(null);

  /* Observable driverId */
  private _bookingId = new BehaviorSubject<any>(null);
  /* Observable driverId */
  private _franchiseId = new BehaviorSubject<any>(null);

  /* Observable Loading Flag */
  private _loadingFlag = new BehaviorSubject<any>(null);

  /* Observable Loading Flag */
  private _vehicleFlag = new BehaviorSubject<any>(null);

  /* logoutFlag for token expiry */
  private _logoutFlag = new BehaviorSubject<any>(false);

  /* Observable for country and state , load it at beginning so that every time we dont have to query*/
  private _country = new BehaviorSubject<any>(null);
  private _state = new BehaviorSubject<any>(null);

  /* Observable gender list */
  private _gender = new BehaviorSubject<any>(null);

  private _vehicleId=new BehaviorSubject<any>(null);
  private _customerId=new BehaviorSubject<any>(null);
  

  /* Observable navItem stream */
  navItem$ = this._navItemSource.asObservable();

  franchise$ = this._franchise.asObservable();

  /* Get OTP Info to next screen */
  otp$ = this._otpInfo.asObservable();

  /* Get OTP Info to next screen */
  driverId$ = this._driverId.asObservable();

  gender$ = this._gender.asObservable();

  /* Get Goods Type and weight List */
  goodsList$ = this._goodsType.asObservable();
  weight$ = this._weightList.asObservable();

  /* Get OTP Info to next screen */
  franchiseId$ = this._franchiseId.asObservable();

  /* Get OTP Info to next screen */
  source$ = this._source.asObservable();

  /* Get OTP Info to next screen */
  destination$ = this._destination.asObservable();

  bookingId$ = this._bookingId.asObservable();
  customerId$ = this._customerId.asObservable();

  /* Get OTP Info to next screen */
  vehicleId$ = this._vehicleId.asObservable();

  /* Common Loading for all pages */
  vehicleFlag$ = this._vehicleFlag.asObservable();

  /* Common Loading for all pages */
  loadingFlag$ = this._loadingFlag.asObservable();

  /* logoutFlag for token expiry */
  logoutFlag$ = this._logoutFlag.asObservable();

  /* get Country ans state only for India */
  country$ = this._country.asObservable();
  state$ = this._state.asObservable();


  // service command
  changeScreen(number) {
    this._navItemSource.next(number);
  }

  otpInfo(item) {
    this._otpInfo.next(item);
  }
  genderList(item) {
    this._gender.next(item);
  }
  getFranchiseactiveId(item){
    this._franchise.next(item);
  }

  setLoadingFlag(flag) {
    this._loadingFlag.next(flag);
  }
  setOnSelectionVehicle(item) {
    this._vehicleFlag.next(item);
  }

  setLogoutFlag(flag) {
    this._logoutFlag.next(flag);
  }
  getBookingId(item) {
    this._bookingId.next(item);
  }
  getDriverId(item) {
    this._driverId.next(item);
  }
  setVehileId(item) {
    this._vehicleId.next(item);
  }
  setCustomerId(item) {
    this._customerId.next(item);
  }
 
  getFranchiseId(item) {
    this._franchiseId.next(item);
  }
  getSource(item) {
    this._source.next(item);
  }
  getDestination(item) {
    this._destination.next(item);
  }
  setCountry(item) {
    this._country.next(item);
  }

  setState(item) {
    this._state.next(item);
  }
  goodsType(item) {
    this._goodsType.next(item);
  }

  weightList(item) {
    this._weightList.next(item);
  }
  
  private companyStatus :boolean;
  // private companyUuId = new BehaviorSubject<any>(null);
  // companyuuId$ = this._companyuuId.asObservable();
  // setItem(key: string, value: string): void;
  // [name: string]: any;
  private companyUuId :string; 
  getCompanyUuId() {
   return this.companyUuId;
  }
  setCompanyUuId(item){
    this.companyUuId=item;
  }
  getCompanyStatus() {
    return this.companyStatus;
   }
  setCompanyStatus(item){
    this.companyStatus=item;
  }
  private pageSize :number; 
  getPageSize() {
   return this.pageSize;
  }
  setPageSize(item){
    this.pageSize=item;
  }
  private pageNo :number; 
  getpageNo() {
   return this.pageNo;
  }
  setpageNo(item){
    this.pageNo=item;
  }
private totalRow:number;
getTotalRow(){
  return this.totalRow;
}
setTotalRow(item){
 this.totalRow=item;
}
private totalPage: number;
getTotalPage(){
  return this.totalPage;
}
setTotalPage(item){
  this.totalPage=item;
}
// setCompanyUuId(item) {
  //   this._companyuuId.next(item);
  // }
  // setCompanyUuId(key,data) {
  //   localStorage.setItem(key, JSON.stringify(data));
  // }

  // getCompanyUuId(key) {
  //   return JSON.parse(localStorage.getItem(key));
  // }

}