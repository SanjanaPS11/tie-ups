import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorHandler, HandleError } from 'src/app/http-error-handler.service';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OperationFranchiselistService {
  handlerError: HandleError;

  constructor(
    private http:HttpClient,
    httpErrorHandler:HttpErrorHandler) {
      this.handlerError = httpErrorHandler.createHandleError('OperationFranchiselistService')
    }
    
    
   
  OperationFranchiselist(url){
    return this.http.get(url)
    .pipe(
      map(this.extractData),
      catchError(this.handlerError('OperationFranchiselist',[]))
    )
  }
  OperationFranchiseactivelist(url){
    return this.http.get(url)
    .pipe(
      map(this.extractData),
      catchError(this.handlerError('OperationFranchiseactivelist',[]))
    )
  }
  OperationFranchiseapprovallist(url){
    return this.http.get(url)
    .pipe(
      map(this.extractData),
      catchError(this.handlerError('OperationFranchiseapprovallist',[]))
    )
  }
  OperationFranchiseFollowupslist(url){
    return this.http.get(url)
    .pipe(
      map(this.extractData),
      catchError(this.handlerError('OperationFranchiseFollowupslist',[]))
    )
  }
  OperationFranchiseInprocesslist(url){
    return this.http.get(url)
    .pipe(
      map(this.extractData),
      catchError(this.handlerError('OperationFranchiseInprocess',[]))
    )
  }
  OperationFranchiseOnholdlist(url){
    return this.http.get(url)
    .pipe(
      map(this.extractData),
      catchError(this.handlerError('OperationFranchiseOnholdlist',[]))
    )
  }
  OperationFranchisecoomonlist(url){
    return this.http.get(url)
    .pipe(
      map(this.extractData),
      catchError(this.handlerError('OperationFranchisecoomonlist',[]))
    )
  }
  private extractData(res: Response){
    let body = res;
    return body || {};
  }
}
