import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationFranchiseComponent } from './operation-franchise.component';

describe('OperationFranchiseComponent', () => {
  let component: OperationFranchiseComponent;
  let fixture: ComponentFixture<OperationFranchiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationFranchiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationFranchiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
