import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationEnquirydashboardComponent } from './operation-enquirydashboard.component';

describe('OperationEnquirydashboardComponent', () => {
  let component: OperationEnquirydashboardComponent;
  let fixture: ComponentFixture<OperationEnquirydashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationEnquirydashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationEnquirydashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
