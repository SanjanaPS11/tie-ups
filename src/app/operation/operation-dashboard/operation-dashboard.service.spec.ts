import { TestBed } from '@angular/core/testing';

import { OperationDashboardService } from './operation-dashboard.service';

describe('OperationDashboardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OperationDashboardService = TestBed.get(OperationDashboardService);
    expect(service).toBeTruthy();
  });
});
