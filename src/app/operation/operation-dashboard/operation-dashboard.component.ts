import { Component, OnInit } from '@angular/core';
import { OperationDashboardService } from './operation-dashboard.service';
import { AppSettings } from 'src/app/constants/constants.module';

@Component({
  selector: 'app-operation-dashboard',
  templateUrl: './operation-dashboard.component.html',
  styleUrls: ['./operation-dashboard.component.css']
})
export class OperationDashboardComponent implements OnInit {
  // fleetCount: any;
  // franchiseCount: any;
  // warehouseCount: any;
  // totalenquiryCount: any;
  // customerCount: any;
  driverCount: any;
  labourCount:any;
  vehicleCount:any;
  boardingCount:any;
  //http://localhost:8085/demo/api/v2/dashboard/driver/requestedBy/SELF/status/PENDING&stateId=2
//private operationDashboardservice:OperationDashboardService ....contructor
  constructor(private operationDashboardservice:OperationDashboardService) { }
  public url = 'http://localhost:8085/demo/api/v2/dashboard/';
  public url1='/requestedBy/SELF/status/PENDING&stateId=';
  public boardingurl='http://localhost:8085/demo/api/v2/dashboard/boardingrequest/count/stateId=';
  ngOnInit() {
    this.operationDashboardservice.getOperationDashboard(this.url+'driver'+this.url1+2)
     .subscribe(response=> {
       this.driverCount={...response};
       this.driverCount=this.driverCount.data;
       console.log(this.driverCount)
     })
     this.operationDashboardservice.getOperationDashboard(this.url+'labour'+this.url1+1)
     .subscribe(response=> {
       this.labourCount={...response};
       this.labourCount=this.labourCount.data;
       console.log(this.labourCount)
     })
     this.operationDashboardservice.getOperationDashboard(this.url+'vehicle'+this.url1+1)
     .subscribe(response=> {
       this.vehicleCount={...response};
       this.vehicleCount=this.vehicleCount.data;
       console.log(this.vehicleCount)
     })
     this.operationDashboardservice.getOperationDashboard(this.boardingurl+1)
     .subscribe(response=> {
       this.boardingCount={...response};
       this.boardingCount=this.boardingCount.data;
       console.log(this.boardingCount)
     })
  //this.operationDashboardservice.getOperationDashboard(AppSettings.TIEUP_ENDPOINT+"getTotalOTFleet/1132")
  //   .subscribe(response => {
  //     this.fleetCount={...response};
  //     this.fleetCount=this.fleetCount.data;
  //     console.log(this.fleetCount)
      
  //   })
  //   this.operationDashboardservice.getOperationDashboard(AppSettings.TIEUP_ENDPOINT+"getTotalOTFranchise/1132")
  //   .subscribe(response=> {
  //     this.franchiseCount={...response};
  //     this.franchiseCount=this.franchiseCount.data;
  //     console.log(this.franchiseCount)
  //   })
  //   this.operationDashboardservice.getOperationDashboard(AppSettings.TIEUP_ENDPOINT+"getTotalOTWarehouse/1132")
  //   .subscribe(response=> {
  //     this.warehouseCount={...response};
  //     this.warehouseCount=this.warehouseCount.data;
  //     console.log(this.franchiseCount)
  //   })
  //   this.operationDashboardservice.getOperationDashboard(AppSettings.TIEUP_ENDPOINT+"getTotalOTBoardingRequests/1132")
  //   .subscribe(response=> {
  //     this.totalenquiryCount={...response};
  //     this.totalenquiryCount=this.totalenquiryCount.data;
  //     console.log(this.totalenquiryCount)
  //   })
  //  this.operationDashboardservice.getOperationDashboard(AppSettings.COMMON_ENDPOINT+"viewDashboardcustomer?")
  //  .subscribe(response=> {
  //    this.customerCount={...response};
  //    this.customerCount=this.customerCount.data[0];
  //    console.log(this.customerCount)
  //  })
  }


}
