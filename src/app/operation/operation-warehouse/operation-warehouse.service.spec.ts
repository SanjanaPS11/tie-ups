import { TestBed } from '@angular/core/testing';

import { OperationWarehouseService } from './operation-warehouse.service';

describe('OperationWarehouseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OperationWarehouseService = TestBed.get(OperationWarehouseService);
    expect(service).toBeTruthy();
  });
});
