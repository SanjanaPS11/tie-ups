import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationWarehouseComponent } from './operation-warehouse.component';

describe('OperationWarehouseComponent', () => {
  let component: OperationWarehouseComponent;
  let fixture: ComponentFixture<OperationWarehouseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationWarehouseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationWarehouseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
