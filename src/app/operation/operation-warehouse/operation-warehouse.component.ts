import { Component, OnInit } from '@angular/core';
import { OperationWarehouseService } from './operation-warehouse.service';
import { AppSettings } from 'src/app/constants/constants.module';

@Component({
  selector: 'app-operation-warehouse',
  templateUrl: './operation-warehouse.component.html',
  styleUrls: ['./operation-warehouse.component.css']
})
export class OperationWarehouseComponent implements OnInit {
  warehousedisplayColumns: string[]= ['id', 'companyName', 'mobileNumber', 'emailId', 'yearOfContract','Action'];
  
  tabList=[
    {'id':0, 'label': 'ACTIVE'},
    {'id':1, 'label': 'APPROVAL'},
    {'id':2, 'label': 'FOLLOW-UPS'},
    {'id':3, 'label': 'INPROCESS'},
    {'id':4, 'label': 'ON-HOLD'},
  ];
  operationWarehouse: any;
  operationWarehouseactivelist: any;
  operationWarehouseapprovallist: any;
  operationWarehouseFollowupslist: any;
  operationWarehouseInprocesslist: any;
  operationWarehouseOnholdlist: any;

  constructor(private operationWarehouseService:OperationWarehouseService) { }

  ngOnInit() {

      this.operationWarehouseService.OperationWarehouselist(AppSettings.TIEUP_ENDPOINT+"warehouseList1/state/1132")
      .subscribe(response => {
        this.operationWarehouseactivelist={...response};
        this.operationWarehouseactivelist=this.operationWarehouseactivelist.data;
        console.log(this.operationWarehouseactivelist)
        })

      this.operationWarehouseService.OperationWarehouseactivelist(AppSettings.TIEUP_ENDPOINT+"warehouseList1/state/1132")
      .subscribe(response => {
        this.operationWarehouseapprovallist={...response};
        this.operationWarehouseapprovallist=this.operationWarehouseapprovallist.data;
        console.log(this.operationWarehouseapprovallist)
          }) 

      this.operationWarehouseService.OperationWarehouseapprovallist(AppSettings.TIEUP_ENDPOINT+"warehouseList/state/1132")
      .subscribe(response => {
        this.operationWarehouseapprovallist={...response};
        this.operationWarehouseapprovallist=this.operationWarehouseapprovallist.data;
        console.log(this.operationWarehouseapprovallist)
        })

      this.operationWarehouseService.OperationWarehouseFollowupslist(AppSettings.TIEUP_ENDPOINT+"boardingrequest/status1/state/4/1132")
      .subscribe(response => {
        this.operationWarehouseFollowupslist={...response};
        this.operationWarehouseFollowupslist=this.operationWarehouseFollowupslist.data;
        console.log(this.operationWarehouseFollowupslist)
        })  
        
      this.operationWarehouseService.OperationWarehouseInprocesslist(AppSettings.TIEUP_ENDPOINT+"boardingrequest/status2/state/4/1132")
      .subscribe(response => {
        this.operationWarehouseInprocesslist={...response};
        this.operationWarehouseInprocesslist=this.operationWarehouseInprocesslist.data;
        console.log(this.operationWarehouseInprocesslist)
        }) 

      this.operationWarehouseService.OperationWarehouseOnholdlist(AppSettings.TIEUP_ENDPOINT+"boardingrequest/status4/state/4/1132")
      .subscribe(response => {
        this.operationWarehouseOnholdlist={...response};
        this.operationWarehouseOnholdlist=this.operationWarehouseOnholdlist.data;
        console.log(this.operationWarehouseOnholdlist)
        })
          
           
  }

}
