import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorHandler } from 'src/app/http-error-handler.service';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OperationFranchiseenquiryService {
  handlerError: any;

  constructor( private http:HttpClient,
    httpErrorHandler:HttpErrorHandler) { 
      this.handlerError = httpErrorHandler.createHandleError('OperationFranchiseenquiryService')
    }
    OperationFranchiseenquiryService(url){
      return this.http.get(url)
      .pipe(
        map(this.extractData),
        catchError(this.handlerError('OperationFranchiseenquiryService',[]))
      )
    }
    private extractData(res: Response){
      let body = res;
      return body || {};
    }
    
}
