import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationFranchiseViewComponent } from './operation-franchise-view.component';

describe('OperationFranchiseViewComponent', () => {
  let component: OperationFranchiseViewComponent;
  let fixture: ComponentFixture<OperationFranchiseViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationFranchiseViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationFranchiseViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
