import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationDriverrequestComponent } from './operation-driverrequest.component';

describe('OperationDriverrequestComponent', () => {
  let component: OperationDriverrequestComponent;
  let fixture: ComponentFixture<OperationDriverrequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationDriverrequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationDriverrequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
