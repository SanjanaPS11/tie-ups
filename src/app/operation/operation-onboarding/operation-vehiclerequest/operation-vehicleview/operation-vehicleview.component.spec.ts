import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationVehicleviewComponent } from './operation-vehicleview.component';

describe('OperationVehicleviewComponent', () => {
  let component: OperationVehicleviewComponent;
  let fixture: ComponentFixture<OperationVehicleviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationVehicleviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationVehicleviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
