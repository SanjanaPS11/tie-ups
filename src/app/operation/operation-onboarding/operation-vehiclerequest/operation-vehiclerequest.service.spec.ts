import { TestBed } from '@angular/core/testing';

import { OperationVehiclerequestService } from './operation-vehiclerequest.service';

describe('OperationVehiclerequestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OperationVehiclerequestService = TestBed.get(OperationVehiclerequestService);
    expect(service).toBeTruthy();
  });
});
