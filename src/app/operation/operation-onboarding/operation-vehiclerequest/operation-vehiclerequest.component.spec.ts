import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationVehiclerequestComponent } from './operation-vehiclerequest.component';

describe('OperationVehiclerequestComponent', () => {
  let component: OperationVehiclerequestComponent;
  let fixture: ComponentFixture<OperationVehiclerequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationVehiclerequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationVehiclerequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
