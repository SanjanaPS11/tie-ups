import { TestBed } from '@angular/core/testing';

import { OperationLabourrequestService } from './operation-labourrequest.service';

describe('OperationLabourrequestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OperationLabourrequestService = TestBed.get(OperationLabourrequestService);
    expect(service).toBeTruthy();
  });
});
