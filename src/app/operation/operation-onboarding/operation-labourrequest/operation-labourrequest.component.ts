import { Component, OnInit, ViewChild } from '@angular/core';
import { OperationLabourrequestService } from './operation-labourrequest.service';
import { AppSettings } from 'src/app/constants/constants.module';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Globalservice } from 'src/app/service/globals.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-operation-labourrequest',
  templateUrl: './operation-labourrequest.component.html',
  styleUrls: ['./operation-labourrequest.component.css']
})
export class OperationLabourrequestComponent implements OnInit {
  driverrequestdisplayColumns: string[]= ['labourId', 'firstName', 'dob', 'mobileNumber', 'cityName', 'creationDate','Action'];
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  operationLabourrequestlist: any;

  constructor(private operationLabourrequestService:OperationLabourrequestService, private globalService:Globalservice, private router:Router) { }

  ngOnInit() {
    this.operationLabourrequestService.OperationLabourrequestlist(AppSettings.TIEUP_ENDPOINT+"onboardstatelabourlist/1132")
      .subscribe(response => {
        this.operationLabourrequestlist={...response};
        this.operationLabourrequestlist=this.operationLabourrequestlist.data;
        console.log(this.operationLabourrequestlist)
        this.pagination();
      });
  }
  viewLabour(element){
    this.globalService.getLabourId(element);
    this.router.navigate(['/labourView']);

  }

  private pagination() {
    this.operationLabourrequestlist = new MatTableDataSource<any>(this.operationLabourrequestlist);
    this.operationLabourrequestlist.paginator = this.paginator;
    this.operationLabourrequestlist.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.operationLabourrequestlist.filter = filterValue.trim().toLowerCase();

    if (this.operationLabourrequestlist.paginator) {
      this.operationLabourrequestlist.paginator.firstPage();
    }

}
}
