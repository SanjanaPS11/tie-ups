import { Component, OnInit, ViewChild } from '@angular/core';
import { OperationFleetenquiryService } from './operation-fleetenquiry.service';
import { AppSettings } from 'src/app/constants/constants.module';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-operation-fleetenquiry',
  templateUrl: './operation-fleetenquiry.component.html',
  styleUrls: ['./operation-fleetenquiry.component.css']
})
export class OperationFleetenquiryComponent implements OnInit {
  fleetenquirydisplayColumns: string[]= ['requestNumber', 'name', 'mobileNumber', 'email', 'message','creationDate','Action'];
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  operationFleetlist: any;

  constructor(private operationFleetenquiryService:OperationFleetenquiryService) { }

  ngOnInit() {
    this.operationFleetenquiryService.OperationFleetenquiryService(AppSettings.TIEUP_ENDPOINT+"getRequestStateListCSVDetails/3/1132")
      .subscribe(response => {
        this.operationFleetlist={...response};
        this.operationFleetlist=this.operationFleetlist.data;
        console.log(this.operationFleetlist)
        this.pagination();
      });
    }

      private pagination() {
        this.operationFleetlist = new MatTableDataSource<any>(this.operationFleetlist);
        this.operationFleetlist.paginator = this.paginator;
        this.operationFleetlist.sort = this.sort;
      }
    
      applyFilter(filterValue: string) {
        this.operationFleetlist.filter = filterValue.trim().toLowerCase();
    
        if (this.operationFleetlist.paginator) {
          this.operationFleetlist.paginator.firstPage();
        }
  
  }

}
