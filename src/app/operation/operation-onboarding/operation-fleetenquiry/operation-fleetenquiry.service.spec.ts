import { TestBed } from '@angular/core/testing';

import { OperationFleetenquiryService } from './operation-fleetenquiry.service';

describe('OperationFleetenquiryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OperationFleetenquiryService = TestBed.get(OperationFleetenquiryService);
    expect(service).toBeTruthy();
  });
});
