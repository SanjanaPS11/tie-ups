import { Component, OnInit, ViewChild } from '@angular/core';
import { OperationCustomerviewService } from './operation-customerview.service';
import { AppSettings } from 'src/app/constants/constants.module';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';


@Component({
  selector: 'app-operation-customer',
  templateUrl: './operation-customer.component.html',
  styleUrls: ['./operation-customer.component.css']
})
export class OperationCustomerComponent implements OnInit {
  customerdisplayColumns: string[]= ['firstName', 'mobileNumber', 'emailId', 'creationDate'];
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  

  tabList=[
    {'id':0, 'label': 'ACTIVE'},
    {'id':1, 'label': 'INACTIVE'},
    
  ];
  OperationCustomerviewService: any;
  operationactiveCustomerview: any;
  operationinactiveCustomerview: any;
  

  constructor(private operationCustomerviewService:OperationCustomerviewService) { }

  ngOnInit() {
    this.operationCustomerviewService.OperationCustomerview(AppSettings.COMMON_ENDPOINT+"active/Customer")
      .subscribe(response => {
        this.operationactiveCustomerview={...response};
        this.operationactiveCustomerview=this.operationactiveCustomerview.data;
        console.log(this.operationactiveCustomerview)
        this.pagination();
        
      });

      // this.operationCustomerviewService.OperationCustomerview(AppSettings.COMMON_ENDPOINT+"disable/Customer")
      // .subscribe(response => {
      //   this.operationinactiveCustomerview={...response};
      //   this.operationinactiveCustomerview=this.operationinactiveCustomerview.data;
      //   console.log(this.operationinactiveCustomerview)
      //   this.pagination();.
        
      // });
  }
  private pagination() {
    this.operationactiveCustomerview = new MatTableDataSource<any>(this.operationactiveCustomerview );
    this.operationactiveCustomerview.paginator = this.paginator;
    this.operationactiveCustomerview.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.operationactiveCustomerview.filter = filterValue.trim().toLowerCase();

    if (this.operationactiveCustomerview.paginator) {
      this.operationactiveCustomerview.paginator.firstPage();
    }
 

}

}
