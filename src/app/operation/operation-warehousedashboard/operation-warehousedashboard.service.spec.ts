import { TestBed } from '@angular/core/testing';

import { OperationWarehousedashboardService } from './operation-warehousedashboard.service';

describe('OperationWarehousedashboardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OperationWarehousedashboardService = TestBed.get(OperationWarehousedashboardService);
    expect(service).toBeTruthy();
  });
});
