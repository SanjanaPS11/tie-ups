import { Component, OnInit } from '@angular/core';
import { OperationWarehousedashboardService } from './operation-warehousedashboard.service';
import { AppSettings } from 'src/app/constants/constants.module';

@Component({
  selector: 'app-operation-warehousedashboard',
  templateUrl: './operation-warehousedashboard.component.html',
  styleUrls: ['./operation-warehousedashboard.component.css']
})
export class OperationWarehousedashboardComponent implements OnInit {
  totalwarehouseCount: any;
  activewarehouseCount: any;
  inactivewarehouseCount: any;

  constructor(private operationWarehousedashboardService:OperationWarehousedashboardService) { }

  ngOnInit() {

    this.operationWarehousedashboardService.getOperationWarehousedashboard(AppSettings.TIEUP_ENDPOINT+"getTotalOTWarehouse/1132")
    .subscribe(response => {
      this.totalwarehouseCount={...response};
      this.totalwarehouseCount=this.totalwarehouseCount.data;
      console.log(this.totalwarehouseCount)
      
    })

    this.operationWarehousedashboardService.getOperationWarehousedashboard(AppSettings.TIEUP_ENDPOINT+"viewOTWarehouseActivate/1132")
    .subscribe(response => {
      this.activewarehouseCount={...response};
      this.activewarehouseCount=this.activewarehouseCount.data;
      console.log(this.activewarehouseCount)
      
    })

    this.operationWarehousedashboardService.getOperationWarehousedashboard(AppSettings.TIEUP_ENDPOINT+"viewOTWarehouseRegistration/1132")
    .subscribe(response => {
      this.inactivewarehouseCount={...response};
      this.inactivewarehouseCount=this.inactivewarehouseCount.data;
      console.log(this.inactivewarehouseCount)
      
    })
    
  }

}
