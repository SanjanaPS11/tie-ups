import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationBiddingComponent } from './operation-bidding.component';

describe('OperationBiddingComponent', () => {
  let component: OperationBiddingComponent;
  let fixture: ComponentFixture<OperationBiddingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationBiddingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationBiddingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
