import { TestBed } from '@angular/core/testing';

import { OperationFleetService } from './operation-fleet.service';

describe('OperationFleetService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OperationFleetService = TestBed.get(OperationFleetService);
    expect(service).toBeTruthy();
  });
});
