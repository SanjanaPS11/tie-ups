import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorHandler } from 'src/app/http-error-handler.service';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OperationFleetService {
  handlerError: any;

  constructor( private http:HttpClient,
    httpErrorHandler:HttpErrorHandler) {
      this.handlerError = httpErrorHandler.createHandleError('OperationFleetService')
     }

     OperationFleetlist(url){
      return this.http.get(url)
      .pipe(
        map(this.extractData),
        catchError(this.handlerError('OperationFleetlist',[]))
      )
    }

    OperationFleetactivelist(url){
      return this.http.get(url)
      .pipe(
        map(this.extractData),
        catchError(this.handlerError('OperationFleetactivelist',[]))
      )
    }
    OperationFleetapprovallist(url){
      return this.http.get(url)
      .pipe(
        map(this.extractData),
        catchError(this.handlerError('OperationFleetapprovallist',[]))
      )
    }
    OperationFleetFollowupslist(url){
      return this.http.get(url)
      .pipe(
        map(this.extractData),
        catchError(this.handlerError('OperationFleetFollowupslist',[]))
      )
    }
    OperationFleetInprocesslist(url){
      return this.http.get(url)
      .pipe(
        map(this.extractData),
        catchError(this.handlerError('OperationFleetInprocess',[]))
      )
    }
    OperationFleetOnholdlist(url){
      return this.http.get(url)
      .pipe(
        map(this.extractData),
        catchError(this.handlerError('OperationFleetOnholdlist',[]))
      )
    }
    private extractData(res: Response){
      let body = res;
      return body || {};
    }
}
