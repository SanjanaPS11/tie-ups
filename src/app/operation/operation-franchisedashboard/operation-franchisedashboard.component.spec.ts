import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationFranchisedashboardComponent } from './operation-franchisedashboard.component';

describe('OperationFranchisedashboardComponent', () => {
  let component: OperationFranchisedashboardComponent;
  let fixture: ComponentFixture<OperationFranchisedashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationFranchisedashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationFranchisedashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
