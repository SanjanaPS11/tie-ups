import { Component, OnInit } from '@angular/core';
import { AppSettings } from 'src/app/constants/constants.module';
import { OperationfranchisedashboardService } from './operationfranchisedashboard.service';

@Component({
  selector: 'app-operation-franchisedashboard',
  templateUrl: './operation-franchisedashboard.component.html',
  styleUrls: ['./operation-franchisedashboard.component.css']
})
export class OperationFranchisedashboardComponent implements OnInit {
  operationFracnhisedashboard: any;
  franchiseCount: any;
  francshieactivecount: any;
  francshieinactivecount: any;
  

  constructor(private operationFracnhisedashboardservice:OperationfranchisedashboardService ) {}

  ngOnInit() {
    this.operationFracnhisedashboardservice.getOperationFranchisedashboard(AppSettings.TIEUP_ENDPOINT+"getTotalOTFranchise/1132")
    .subscribe(response => {
      this.franchiseCount={...response};
      this.franchiseCount=this.franchiseCount.data;
      console.log(this.franchiseCount)
      
    })
  
  this.operationFracnhisedashboardservice.getOperationFranchisedashboard(AppSettings.TIEUP_ENDPOINT+"getTotalOTFleet/1132")
    .subscribe(response => {
      this.francshieactivecount={...response};
      this.francshieactivecount=this.francshieactivecount.data;
      console.log(this.francshieactivecount)
      
    })
    this.operationFracnhisedashboardservice.getOperationFranchisedashboard(AppSettings.TIEUP_ENDPOINT+"getTotalOTFleet/1132")
    .subscribe(response => {
      this.francshieinactivecount={...response};
      this.francshieinactivecount=this.francshieinactivecount.data;
      console.log(this.francshieinactivecount)
      
    })
  }

}
