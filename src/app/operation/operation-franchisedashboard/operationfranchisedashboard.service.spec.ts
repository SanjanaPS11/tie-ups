import { TestBed } from '@angular/core/testing';

import { OperationfranchisedashboardService } from './operationfranchisedashboard.service';

describe('OperationfranchisedashboardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OperationfranchisedashboardService = TestBed.get(OperationfranchisedashboardService);
    expect(service).toBeTruthy();
  });
});
