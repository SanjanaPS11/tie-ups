import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationBookingComponent } from './operation-booking.component';

describe('OperationBookingComponent', () => {
  let component: OperationBookingComponent;
  let fixture: ComponentFixture<OperationBookingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationBookingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationBookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
