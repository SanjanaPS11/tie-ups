import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";

import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'app-home-nav',
  templateUrl: './home-nav.component.html',
  styleUrls: ['./home-nav.component.css']
})
export class HomeNavComponent implements OnInit {

  public logoUrl: String;

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
    this.logoUrl = 'https://s3.ap-south-1.amazonaws.com/common-mmg/MMG+LOGOv2.png';
  }

  openModal (){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(LoginComponent,
      dialogConfig);
  }

}
