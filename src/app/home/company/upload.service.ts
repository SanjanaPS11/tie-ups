import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse }
  from '@angular/common/http';
  import { Observable} from 'rxjs/internal/Observable';
  import { BehaviorSubject } from 'rxjs';
import { CompanyService } from './company.service';
import { finalize } from 'rxjs/operators';
// @Injectable({
//   providedIn: 'root'
// })
@Injectable()
export class UploadService implements HttpInterceptor {
 constructor(public companyService:CompanyService){}
  intercept(httpRequest: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    //this.companyService.isLoading.next(true);
    console.log( "Sending request interceptor ");
    console.log(httpRequest.url);
  //   let modifiedRequest=httpRequest.clone({
  //     headers:httpRequest.headers.append('auth','abc'),
  //    url:"fdfgf",
  //  params:httpRequest.params.append('hi','hello world')
  //  });
    
    return next.handle(httpRequest);
  }
  //   .pipe(
  //     finalize(
  //       ()=>{
  //         this.companyService.isLoading.next(false);
  //       }
  //     )
  //   );
  // }
}
