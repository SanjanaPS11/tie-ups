import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveDeactivateCompanyComponent } from './active-deactivate-company.component';

describe('ActiveDeactivateCompanyComponent', () => {
  let component: ActiveDeactivateCompanyComponent;
  let fixture: ComponentFixture<ActiveDeactivateCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActiveDeactivateCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveDeactivateCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
