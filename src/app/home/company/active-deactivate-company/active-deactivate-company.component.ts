import { Component, Inject, OnInit } from '@angular/core';
import { CompanyService } from '../company.service';
import { Globalservice } from 'src/app/service/globals.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-active-deactivate-company',
  templateUrl: './active-deactivate-company.component.html',
  styleUrls: ['./active-deactivate-company.component.css']
})
export class ActiveDeactivateCompanyComponent implements OnInit {

  constructor(private companyService:CompanyService, private globalService: Globalservice,
    private dialogRef: MatDialogRef<ActiveDeactivateCompanyComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: any){}

  button: boolean;
  title:String;
 
  ngOnInit() {
    
    let status=this.globalService.getCompanyStatus();
    this.button=status;
   console.log(status)
   if(status==true)
   {
      this.title="Deactivate";
   }
   else
        this.title="Activate";
 
  }

  ActiveOrDeativate()
  { 
    let status=this.globalService.getCompanyStatus();
    let uuId=this.globalService.getCompanyUuId();   
   
    console.log(status,uuId);
    this.globalService.setCompanyUuId(0);
    this.companyService.patchCompanyStatus(status,uuId)
    .subscribe(response => {
      console.log(status);
    });
    this.dialogRef.close();
  }
  close() {
  
    this.dialogRef.close();
  }
}
