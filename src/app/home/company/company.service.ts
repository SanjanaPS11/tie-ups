import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BehaviorSubject} from 'rxjs';
import { HttpRequest, HttpEvent} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {
 public isLoading:BehaviorSubject<boolean>=new BehaviorSubject<boolean>(false);
  constructor(private http: HttpClient) { }
  // private _companyType = new BehaviorSubject<any>(null);
  // companyType$ = this._companyType.asObservable();
  // companyTypeList(item) {
  //   this._companyType.next(item);
  // }

  baseUrl = "http://localhost:8085/demo/api/v1";

  private headers = { "Access-Control-Allow-Origin": "*", "Access-Control-Allow-Methods": "POST, PUT,PATCH, GET, OPTIONS, DELETE", "Access-Control-Allow-Headers": "Authorization,Content-Type,X-API-KEY", "Content-Type": "application/json", "X-API-KEY": "MMGATPL" }

 getLogin(data) {
    return this.http.get(`${this.baseUrl}/login`,data);
  }
  createMessage(message: Object) {
    return this.http.post(`${this.baseUrl}/message`, message, { 'headers': this.headers });
  }
  getMessage(): Observable<any> {
    return this.http.get(`${this.baseUrl}/message`, { 'headers': this.headers });
  }
 
  deleteMessage(uuId: String) {
    return this.http.delete(`${this.baseUrl}/message/${uuId}`, { 'headers': this.headers, responseType: 'text' });

  }

  search(value: String): Observable<any> {
    return this.http.get(`${this.baseUrl}/Search/${value}`, { 'headers': this.headers });
  }
  pagination(pageNo: number, pageSize: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/company/pageSize/${pageSize}/pageNo/${pageNo}`, { 'headers': this.headers });
  }
  imageUpload(image: File): Observable<HttpEvent<{ any }>> {
    const headr = { "Access-Control-Allow-Origin": "*", "Access-Control-Allow-Headers": "Authorization,Content-Type,boundary", "X-API-KEY": "MMGATPL" }
    const formData: FormData = new FormData();
    formData.append('image', image, image.name);
    let req = new HttpRequest('POST', `${this.baseUrl}/image/uploadimage`, formData, { headers: new HttpHeaders(headr) });
    return this.http.request(req);
  }
  imageDownload(path: string, uuid: String): Observable<any> {
    const headr = { "Access-Control-Allow-Origin": "*", "Access-Control-Allow-Headers": "Authorization,Content-Type,boundary", "X-API-KEY": "MMGATPL" }

    const formData: FormData = new FormData();
    formData.append('image', path);
    // return this.http.post(`${this.export}`,formData,{'headers' : this.headers});
    let req = new HttpRequest('POST', `${this.baseUrl}/image/downloadimage/${uuid}`, formData, { headers: new HttpHeaders(headr) });
    return this.http.request(req);

  }

  getImage() {
    return this.http.get("http://localhost:8085/demo/api/v1/image/getImage");
  }

  createCompany(company: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}/company`, company, { 'headers': this.headers });
  }

  uploadFile(file: File): Observable<HttpEvent<{ any }>> {
     const headr = { "Access-Control-Allow-Origin": "*", "Access-Control-Allow-Headers": "Authorization,Content-Type,boundary", "X-API-KEY": "MMGATPL" }

    const formData: FormData = new FormData();
    formData.append('file', file);
    let req = new HttpRequest('POST', `${this.baseUrl}/uploadExcel"`, formData, { headers: new HttpHeaders(headr) });
    return this.http.request(req);
  }

  downloadCompanyDetails(path: string): Observable<any> {
    console.log(path);
    const headr = { "Access-Control-Allow-Origin": "*", "Access-Control-Allow-Headers": "Authorization,Content-Type,boundary", "X-API-KEY": "MMGATPL" }

    const formData: FormData = new FormData();
    formData.append('file', path);

    let req = new HttpRequest('POST', `${this.baseUrl}/downloadExcel`, formData, { headers: new HttpHeaders(headr) });
    return this.http.request(req);
  }

  getCompany(uuId: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/company/${uuId}`, { 'headers': this.headers });
  }

  getCompanyStatus(status: Boolean): Observable<any> {
    return this.http.get(`${this.baseUrl}/company/status/${status}`, { 'headers': this.headers });
  }
  updateCompany(uuId, data): Observable<Object> {
    return this.http.put(`${this.baseUrl}/company/${uuId}`, data, { 'headers': this.headers });
  }

  patchCompanyStatus(status, uuId) {
    console.log("service")
    return this.http.patch(`${this.baseUrl}/company/isActive/${status}/uuId/${uuId}`, { 'headers': this.headers });
  }
  deleteCompany(uuId: string): Observable<any> {

    return this.http.delete(`${this.baseUrl}/company/${uuId}`, { 'headers': this.headers, responseType: 'text' });
  }

  getAllCompany() {
    return this.http.get(this.baseUrl + '/company');
  }

}












// url="http://localhost:8085/demo/api/v1/uploadExcel";
// export="http://localhost:8085/demo/api/v1/downloadExcel";
// image="http://localhost:8085/demo/api/v1/image/uploadimage";
// imagedownload="http://localhost:8085/demo/api/v1/image/downloadimage";