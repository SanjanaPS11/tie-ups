import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { BehaviorSubject } from 'rxjs';
import { Globalservice } from 'src/app/service/globals.service';
import { CompanyService } from '../company.service';

@Component({
  selector: 'app-create-update-company',
  templateUrl: './create-update-company.component.html',
  styleUrls: ['./create-update-company.component.css']
})
export class CreateUpdateCompanyComponent implements OnInit {
  companyForm: FormGroup;
  companyTypes = [
    { 'id': 0, 'val': ' ' },
    { 'id': 1, 'val': 'Private' },
    { 'id': 2, 'val': 'Public' },
    { 'id': 3, 'val': 'Others' }
  ];
  companyType: { id: number; val: string; };
  controls: {
    [key: string]: AbstractControl;
};
  companyData: string;
  company: any;
  title: string;
  button: boolean;
  uuId: any;
 
  // private companyData = new BehaviorSubject<any>(null);
  constructor(private dialogRef: MatDialogRef<CreateUpdateCompanyComponent>,private globalService: Globalservice,
    private companyService:CompanyService,private formBuilder: FormBuilder) {
      this.companyForm = this.formBuilder.group({
        companyName: new FormControl('', [Validators.required,Validators.pattern('[a-zA-Z ]*')]),
        companyType: new FormControl(null, [Validators.required]),
        emailId: new FormControl('', [Validators.required,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),
        mobileNumber: new FormControl('', [Validators.required,Validators.pattern('^([+][9][1]|[9][1]|[0]){0,1}([4-9]{1})([0-9]{9})$')]),
        established: new FormControl('', [Validators.required, Validators.pattern('(19|20|21)[0-9][0-9]')]),
        mdName: new FormControl('', [Validators.required,Validators.minLength(4)]),
        panNumber: new FormControl('', [Validators.required,Validators.pattern("[A-Z]{5}[0-9]{4}[A-Z]{1}")]),
        gstNumber: new FormControl('', [Validators.required,Validators.pattern("^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$")]),
        webSite:new FormControl('',[Validators.required])
        });
     }
    // {1}[1-9]{1}[0-9]{9}$ BNZAA2318J 
  ngOnInit() {
    this.title = "Register";
    this.button = false;
    this.companyForm.controls['companyType'].setValue(this.companyTypes[0].val)
    this.companyData = this.globalService.getCompanyUuId();
    console.log(this.companyData);
    if (this.companyData) {
      this.title = "Update";
      this.button = true;
        this.companyService.getCompany(this.companyData)
        .subscribe(data => {
          console.log(data)
          this.company ={...data};
          this.company = this.company.data;
          this.companyForm.controls['companyType'].setValue(this.companyTypes[0].val)
          if (this.company.companyType == "Private") {
            this.companyForm.controls['companyType'].setValue(this.companyTypes[1].val)
          } 
          else if(this.company.companyType == "Public") {
            this.companyForm.controls['companyType'].setValue(this.companyTypes[2].val)
          }
          else{
            this.companyForm.controls['companyType'].setValue(this.companyTypes[3].val)
          }
          delete this.company.uuId;
          delete this.company.createdDate;
          delete this.company.modifiedDate;
          delete this.company.isActive;
          console.log(this.company);
          this.companyForm.setValue(this.company);
          //this.companyForm.controls['companyType'].disable()
          
        });
        
    }
   
  }
  submit() {
    let data = this.companyForm.value;
    console.log(this.companyForm)
    this.companyService.createCompany(data).subscribe((response) => {
        alert("Registered successful")
        this.companyForm.reset();
        
        this.dialogRef.close();
      });
  }
  update() {
    let updatedata=this.companyForm.value;
    this.uuId= this.globalService.getCompanyUuId();
 
    console.log(updatedata);
    this.companyService.updateCompany(this.uuId,updatedata)
      .subscribe(data => {
        console.log(data);
        this.companyForm.reset();
      }, error => console.log(error));
      this.globalService.setCompanyUuId(0);
      alert("updated successful")
    this.dialogRef.close();
  }
  close() {
    this.dialogRef.close();
    this.globalService.setCompanyUuId(0);
  }
  public hasError = (controlName: string, errorName: string) => {
    return this.companyForm.controls[controlName].hasError(errorName);
  }
}
   