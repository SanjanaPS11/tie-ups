import { Component, OnInit ,Inject} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog, MatDialogConfig } from "@angular/material";
import { Router } from '@angular/router';
import { CompanyService } from '../company.service';
@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupComponent implements OnInit {
  public title: String;
  public Button: String;

  constructor(private dialogRef: MatDialogRef<PopupComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: any, private router:Router,private companyService:CompanyService) { }

  ngOnInit() {
    if(this.dialogData)
    {
      this.title = this.dialogData.title;
      this.Button=this.dialogData.name;
    }
    else
      this.dialogRef.close();
  }

  close()
  {
    this.dialogRef.close();
   
  }

  save()
  {
   
    this.dialogRef.close(this.title);
  
}
}


// if(this.Button=="Delete")
// {
// console.log(this.Button)
//   this.companyService.deleteCompany(this.dialogData.uuId)
//   .subscribe(
//     data => {
//       console.log("delete")
//       alert("data is deleted sucessfully");
   
//     },
//     error => console.log(error))
// }