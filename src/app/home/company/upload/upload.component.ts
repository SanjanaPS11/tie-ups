import { Component, OnInit, ViewChild } from '@angular/core';
import { CompanyService } from '../company.service';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { AbstractControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {DomSanitizer} from '@angular/platform-browser';
@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
@Injectable({
  providedIn: 'root'
})
export class UploadComponent implements OnInit {
@ViewChild(MatPaginator,{static: false}) paginator: MatPaginator;
@ViewChild(MatSort,{static: false}) sort: MatSort;
  fileForm: FormGroup;
    file: File = null; // Variable to store file 
  registration: any;
  constructor(private domsani:DomSanitizer,private http: HttpClient,private companyService: CompanyService, private formBuilder: FormBuilder) {
    this.fileForm = this.formBuilder.group({
      file: new FormControl('', [Validators.required])
    })
  }
  base64Data: any;
  retrieveResonse: any;
  retrievedImage: any;
  imagePath: any;
  path: any;
  displayedColumns: string[] = ['uuId','imageType','imageName','image','action'];

  ngOnInit() {
    this.getImage();
  }
  onChange(event) {
    this.file = event.target.files[0];
  }
  public onUploadExcel() {
    this.companyService.uploadFile(this.file).subscribe(response => {
      console.log(response)
    });
  }

  downloadExcel() {
    let path = this.path;
    console.log(path);
    this.companyService.downloadCompanyDetails(path)
      .subscribe(response => {
        console.log("Download");
      })
  }

  image(event) {
    this.file = event.target.files[0]
  }
  uploadImage() {
    this.companyService.imageUpload(this.file)
      .subscribe(response => {
        console.log("Download");
      });
      this.getImage()

  }

  downloadImage(uuid) {
    let path = this.imagePath;
    console.log(path);
    this.companyService.imageDownload(path,uuid)
      .subscribe(response => {
        console.log("Download");
        this.pagination();
      });
      this.getImage()
  }
  //registration:any;
  getImage() {
    console.log("Download image");
     this.companyService.getImage()
    .subscribe(response => {
      this.registration = { ...response };
      this.registration = this.registration.data;
      var file = new Blob([this.registration], {type: 'application/pdf'});
    var fileURL = URL.createObjectURL(file);
    window.open(fileURL);
      console.log(fileURL);
      
      
    });
      
         //Make a call to Sprinf Boot to get the Image Bytes.
              //   this.httpClient.get('http://localhost:8080/image/get/' + this.imageName)
              //     .subscribe(
              //       res => {
              //         this.retrieveResonse = res;
              //         this.base64Data = this.retrieveResonse.picByte;
              //         this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
              //       }
              //     );
              // }
   
 }
 get(uuId)
 {
         this.http.get('http://localhost:8085/v1/image/downloadimage' )
                  .subscribe(
                    res => {
                      this.retrieveResonse = res;
                      this.base64Data = this.retrieveResonse.picByte;
                      this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
                    }
                  );
}
 
 private pagination() {
  this.registration = new MatTableDataSource<any>(this.registration);
  this.registration.paginator = this.paginator;
  this.registration.sort = this.sort;
}

applyFilter(filterValue: string) {
  this.registration.filter = filterValue.trim().toLowerCase();
  if (this.registration.paginator) {
    this.registration.paginator.firstPage();
   }
}
}
 
  