import { Component, OnInit ,ViewChild} from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CompanyService } from '../company.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialogConfig } from '@angular/material';
import{ PopupComponent } from 'src/app/home/company/popup/popup.component';
import { MatDialog} from "@angular/material/dialog";


@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {
  messageForm: FormGroup;
@ViewChild(MatPaginator,{static: false}) paginator: MatPaginator;
@ViewChild(MatSort,{static: false}) sort: MatSort;
  constructor(private companyService: CompanyService,private formBuilder: FormBuilder,public dialog: MatDialog) { 
    this.messageForm = this.formBuilder.group({
      roleName: new FormControl('', [Validators.required]),
      messageName: new FormControl('', [Validators.required]),
      message: new FormControl('', [Validators.required]),
     });

  }
message: any;
displayedColumns: string[] = ['uuId','roleId','messageName','message','action'];
company:any;
  ngOnInit() {
    this.reload();
  }
  submit() {
    let data = this.messageForm.value;
    console.log(this.messageForm)
    this.companyService.createMessage(data).subscribe((response) => {
        alert("Registered successful")
        this.messageForm.reset();
      });
  }

  reload()
  {
    this.companyService.getMessage()
    .subscribe(response => {
      this.message = { ...response };
      this.message = this.message.data;
    
      console.log(this.message);  
      this.pagination();
      });
  }

  deleteCompany(uuId: string,companyName: string) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title : 'Are You Sure you want to delete ',
      name:"Delete",
      uuId:uuId
    }

    const dialogRef = this.dialog.open(PopupComponent ,
      dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        if (data == undefined)
          return;
        this.companyService.deleteMessage(uuId)
        .subscribe(
          data => {
            alert("data is deleted sucessfully");
            this.reload();
          },
          error => console.log(error));
       });
    this.reload();
  }

  public hasError = (controlName: string, errorName: string) =>{
    return this.messageForm.controls[controlName].hasError(errorName);
  }

  private pagination() {
    this.message = new MatTableDataSource<any>(this.message);
    this.message.paginator = this.paginator;
    this.message.sort = this.sort;
  }
  applyFilter(filterValue: string) {
    this.message.filter = filterValue.trim().toLowerCase();
    if (this.message.paginator) {
      this.message.paginator.firstPage();
     }
  }
}
