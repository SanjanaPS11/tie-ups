import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { AbstractControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { CompanyService } from './company.service';

import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { PopupGeneralComponent } from 'src/app/popup-general/popup-general.component';
import{ PopupComponent } from 'src/app/home/company/popup/popup.component';
import { CreateUpdateCompanyComponent } from '../company/create-update-company/create-update-company.component';
import { Globalservice } from 'src/app/service/globals.service';
import { ActiveDeactivateCompanyComponent } from '../company/active-deactivate-company/active-deactivate-company.component';
import { UploadComponent } from '../company/upload/upload.component';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent implements OnInit {
  
  companyForm: FormGroup;
  companyTypes = [
    { 'id': 1, 'val': ' ' },
    { 'id': 2, 'val': 'Private' },
    { 'id': 3, 'val': 'Public' },
    { 'id': 4, 'val': 'Others' }
  ];


  paginationSizes = [
    { 'id': 1, 'val': 5},
    { 'id': 2, 'val': 10},
    { 'id': 3, 'val':15 }
  ];
 
  @Input()

  companyType: { id: number; val: string; };
  
  paginationSize: { id: number; val: number; };

  controls: {
    [key: string]: AbstractControl;
};

displayedColumns: string[] = ['companyName', 'companyType','mobileNumber', 'mdName', 'established', 'panNumber', 'gstNumber', 'webSite','emailId','action'];
company:any;
registration: any;

@ViewChild(MatPaginator,{static: false}) paginator: MatPaginator;
@ViewChild(MatSort,{static: false}) sort: MatSort;
  title: string;
  name:string;
  path: any;
  imagePath:any;
  constructor(private companyService: CompanyService,private formBuilder: FormBuilder,private router: Router,
    public dialog: MatDialog, private globalService: Globalservice) 
  { 
    this.companyForm = this.formBuilder.group({
    companyName: new FormControl('', [Validators.required]),
    companyType: new FormControl('', [Validators.required]),
    emailId: new FormControl('', [Validators.required]),
    mobileNumber: new FormControl('', [Validators.required]),
    established: new FormControl('', [Validators.required]),
    mdName: new FormControl('', [Validators.required]),
    panNumber: new FormControl('', [Validators.required]),
    gstNumber: new FormControl('', [Validators.required]),
    webSite:new FormControl('',[Validators.required]),
    paginationSize: new FormControl('')
    });
    
  }
  checked:boolean;
  file: File = null;
  ngOnInit() {
    this.companyForm.controls['companyType'].setValue(this.companyTypes[0].val)
    this.companyForm.controls['paginationSize'].setValue(this.paginationSizes[0].val)
    this.globalService.setPageSize(5);
    this.reload();
  
  }


  isHide: boolean = true ;
  ActiveOrDeativate(status)
  {
    if(status==true){
      this.isHide = false;
    }
    else{
    this.isHide = true;
    }
   
    this.companyService.getCompanyStatus(status)
    .subscribe(response => {
      this.registration = { ...response };
      this.registration = this.registration.data;
      console.log(this.registration);
      this.pagination();
    });
  }


  reload()
  {
    this.isHide = true;
    this.companyService.getAllCompany()
    .subscribe(response => {
      this.registration = { ...response };
      this.registration = this.registration.data;
      var keys = Object.keys(this.registration);
      var len = keys.length;
      console.log(len);  
      this.globalService.setTotalRow(len);
      console.log(this.registration);
      this.pagination();
    });
  }



  addCompany()
  {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title : 'Are You Sure you want to create company?',
      name:"Create"
    }

    const dialogRef = this.dialog.open(PopupComponent,
      dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        if (data == undefined)
          return;
          const dialogRef=this.dialog.open(CreateUpdateCompanyComponent);
          dialogRef.afterClosed().subscribe(result=>{
            this.reload();
            console.log("New Company Information entered");
            
        })
      });
}
 
  deleteCompany(uuId: string,companyName: string) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title : 'Are You Sure you want to delete '+ companyName,
      name:"Delete",
      uuId:uuId
    }

    const dialogRef = this.dialog.open(PopupComponent,
      dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        if (data == undefined)
          return;
        this.companyService.deleteCompany(uuId)
        .subscribe(
          data => {
            alert("data is deleted sucessfully");
            this.reload();
          },
          error => console.log(error));
       });
    this.reload();
  }

  patchCompany(status,uuId,companyName) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    if(status==true){
      this.name="Deactivate"
    }
      else
      {
      this.name="Activate"
    }
    dialogConfig.data = {
      title : 'Are You Sure you want to change status of  '+ companyName,
      name:this.name
    }

    const dialogRef = this.dialog.open(PopupComponent,
      dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        if (data == undefined)
          return;
          if(status==true)
            {
              status= false;

            }
            else{
              status=true;
            }
          console.log(status,uuId);
          this.companyService.patchCompanyStatus(status,uuId)
          .subscribe(response => {
            console.log(status);
            alert("patch sucessfully");
            this.reload();
          });
    });
  }

update(uuId,companyName)
{

const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    
    dialogConfig.data = {
      title : 'Are You Sure you want to update company?' + companyName,
      name:"Update"
    }

    const dialogRef = this.dialog.open(PopupComponent,
      dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        if (data == undefined)
          return;
          this.globalService.setCompanyUuId(uuId);
          const dialogRef=this.dialog.open(CreateUpdateCompanyComponent);
          dialogRef.afterClosed().subscribe(result=>{
            this.reload();
            console.log("update");
            
        })
      }); 
}

  private pagination() {
    this.registration = new MatTableDataSource<any>(this.registration);
    this.registration.paginator = this.paginator;
    this.registration.sort = this.sort;
  }

  search(searchValue: string){
    console.log("search");
    if(searchValue){
   this.companyService.search(searchValue).subscribe(response=>{
    this.registration = { ...response };
    this.registration = this.registration.data;
    console.log(this.registration);
   
   });
   }
    this.pagination();
  }
  applyFilter(filterValue: string) {
    this.registration.filter = filterValue.trim().toLowerCase();
    if (this.registration.paginator) {
      this.registration.paginator.firstPage();
     }
  }
  public hasError = (controlName: string, errorName: string) =>{
    return this.companyForm.controls[controlName].hasError(errorName);
  }

paginations(pageSize:number){
  console.log(pageSize);
  this.globalService.setpageNo(0);
  this.globalService.setPageSize(pageSize);
  let count=this.globalService.getTotalRow();
  const pagesAmount = Math.ceil(count / pageSize );
  this.globalService.setTotalPage(pagesAmount);
  this.companyService.pagination(0,pageSize)
  .subscribe(response => {
    this.registration = { ...response };
    this.registration = this.registration.data;
    console.log(this.registration);
   // this.pagination();
  });
  
  }
  previousPage(){
    console.log("priviouspage");
  //let count=this.globalService.getTotalPage();
  let currentPage=this.globalService.getpageNo();
  let pageSize=this.globalService.getPageSize();
  if(--currentPage>-1){
    this.paginationFun(--currentPage,pageSize);
    this.globalService.setpageNo(--currentPage);
  }
  
  }
  paginationFun(pageNo,pageSize){
    console.log(pageNo);
    this.companyService.pagination(pageNo,pageSize)
    .subscribe(response => {
      this.registration = { ...response };
      this.registration = this.registration.data;
      console.log(this.registration);
     // this.pagination();
    });
    
  }
  nextPage(){
     console.log("nextpage");
    let count=this.globalService.getTotalPage();
    let currentPage=this.globalService.getpageNo();
    let pageSize=this.globalService.getPageSize();
    if(currentPage<count){
      this.paginationFun(++currentPage,pageSize);
      this.globalService.setpageNo(1+currentPage);
    }
  }
}


















// patchCompanyStatus(status,uuId,companyName)
// {
//   console.log(status);
// const dialogConfig = new MatDialogConfig()
//     dialogConfig.disableClose = true;
//     dialogConfig.autoFocus = true;
//     dialogConfig.data = {
//       title : companyName,
//       uuId:uuId,
//       status:status
//     }

//     const dialogRef = this.dialog.open(PopupGeneralComponent,
//       dialogConfig);

//     dialogRef.afterClosed().subscribe(
//       data => {
//         if (data == undefined)
//           return;
           
//            if(status==true)
//             {
//               status= false;
//               console.log(status)
//             this.globalService.setCompanyStatus(status);
//             }
//             else{
//               status=true;
//               console.log(status)
//               this.globalService.setCompanyStatus(status);
//             }
//           const dialogRef=this.dialog.open(ActiveDeactivateCompanyComponent);
         
//           this.globalService.setCompanyUuId(uuId);
//           dialogRef.afterClosed().subscribe(result=>{
//             alert("status  is updated successfully");
//             this.reload();
//             console.log("Patch");
//         })
//       }); 
// }