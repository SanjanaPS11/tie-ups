import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { AppSettings } from '../../constants/constants.module';

import { Globalservice } from '../../service/globals.service';
import { SnackBarService } from '../../service/snackbar.service';
import { ForgotPasswordService } from './forgot-password.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  providers: [ForgotPasswordService],
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  forgotPasswordForm: FormGroup;
  subscription: Subscription;
  mobileNo: any;

  errorMsgs = AppSettings.ERROR_MSGS;

  private passwordConfirming(control: AbstractControl) {
    let parent = control.parent
    if (parent) {
      let password = parent.get('password').value;
      let confirmPassword = control.value;

      if (password != confirmPassword) {
        return { equalsTo : true };
      } else {
        return null
      }
    } else {
      return null;
    }
  }

  constructor(private formBuilder: FormBuilder, private router: Router,
        private forgotPasswordService: ForgotPasswordService, private globalservice: Globalservice,
        private snackBarService: SnackBarService) {
    this.forgotPasswordForm = this.formBuilder.group({
      password: new FormControl('', [Validators.minLength(8), Validators.maxLength(50), Validators.required]),
      confirmPassword: new FormControl('', [Validators.minLength(8), Validators.maxLength(50), Validators.required, this.passwordConfirming]),
    });
  }

  ngOnInit() {
    /* Listen for Mobile Number and email from OTP page */
    this.subscription = this.globalservice.otp$
      .subscribe(response => {
        if(response == null) {
          this.router.navigate(['/validate']);
        }
        else {
          this.mobileNo = response.mobileNumber;
        }
    });


  }

  changePassword() {
    if (this.forgotPasswordForm.valid) {
      this.globalservice.setLoadingFlag(true);
      let data = this.forgotPasswordForm.value;
      data.mobileNumber = this.mobileNo;
      data.roleId = AppSettings.CUSTOMER_ROLE_ID;
      console.log(data);
      this.forgotPasswordService.resetPassword(AppSettings.COMMON_ENDPOINT + 'updatePassword',data)
        .subscribe(response => {
          this.router.navigate(['/mmg']);
          this.snackBarService.openSnackBar("Password Updated Successfully", null);
        },
        err => {
          this.globalservice.setLoadingFlag(false);
        },
        () => {
          this.globalservice.setLoadingFlag(false);
      });
    }
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.forgotPasswordForm.controls[controlName].hasError(errorName);
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    /* prevent memory leak when component is destroyed */
    this.subscription.unsubscribe();
  }

}
