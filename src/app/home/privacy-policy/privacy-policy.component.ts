import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.css']
})
export class PrivacyPolicyComponent implements OnInit {

  policyList = [
    {
      'title': 'SCOPE', 'heading': 'This Privacy Policy applies to all non-public, online and offline, collected, stored,processed, transferred and used Personal Data of MMG customers, vendors, employees, agents, contractors and others who share their Personal Data with us. A copy of this Privacy Policy is located on MMG website at www.movemygoods.in'
    },
    {
      'title': 'INFORMATION WE COLLECT', 'heading': 'We may collect the following information from You:', 'level1': [
        { 'header': 'PERSONAL INFORMATION:', 'content': 'We may collect certain personal information including Your name, gender, address, email ID, mobile number, and DoB.' },
        { 'header': 'SERVICE-RELATED INFORMATION:', 'content': 'We may collect details regarding Your usage of our services, including but not limited to, the type of service requested, the time when the service was provided, the amount charged and distance covered.' },
        { 'header': 'USAGE INFORMATION:', 'content': 'We may collect information relating to Your usage of, and interaction with, our services including but not limited to location data, web logs, IP address and browser information.' }
      ]
    },
    {
      'title': 'SENSITIVE INFORMATION', 'level1': [
        { 'header': 'While recognizing that all Personal Data deserves to be protected, MMG incorporates special precautions and safeguards for any Sensitive Personal Data we may collect. Sensitive Personal Data includes Personal Data specifying medical or health data, racial or ethnic origin, political opinions, religious or philosophical beliefs, trade union membership or information specifying details of a conviction or criminal offense.'},
        { 'header': 'MMG does not collect Sensitive Personal Data regarding its customers or vendors, unless it is required or authorized by law, or unless the customer or vendor has provided its express consent. However, when necessary to support its relationship with its employees, or when required by local law, MMG may collect “Sensitive Personal Data” regarding its employees.'}
      ]
    },
    {
      'title': 'INFORMATION WE COLLECT', 'heading': 'We may collect the following information from You:', 'level1': [
        { 'header': 'PERSONAL INFORMATION:', 'content': 'We may collect certain personal information including Your name, gender, address, email ID, mobile number, and DoB.' },
        { 'header': 'SERVICE-RELATED INFORMATION:', 'content': 'We may collect details regarding Your usage of our services, including but not limited to, the type of service requested, the time when the service was provided, the amount charged and distance covered.' },
        { 'header': 'USAGE INFORMATION:', 'content': 'We may collect information relating to Your usage of, and interaction with, our services including but not limited to location data, web logs, IP address and browser information.' }
      ]
    },
  ];

  constructor() { }

  ngOnInit() {
  }

}
