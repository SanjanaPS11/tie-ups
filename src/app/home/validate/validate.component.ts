import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, Route, ActivatedRoute } from '@angular/router';

import { AppSettings } from '../../constants/constants.module';

import { Globalservice } from '../../service/globals.service';
import { ValidateService } from './validate.service';

@Component({
  selector: 'app-validate',
  templateUrl: './validate.component.html',
  providers: [ValidateService],
  styleUrls: ['./validate.component.css']
})
export class ValidateComponent implements OnInit {

  requestOtpForm: FormGroup;
  submitOtpForm: FormGroup;
  title: String;
  public gotOTP: Boolean;
  public buttonTitle: String;
  public no_email: Boolean;
  private sub: any;

  errorMsgs = AppSettings.ERROR_MSGS;

  constructor(private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute,
    private validateService: ValidateService, private globalservice: Globalservice) { }

  ngOnInit() {

    /* Since we are using same component for driver, user and forgot password. 
    Need to reload the init for every route change */
    this.sub = this.route.params.subscribe(params => {
      this.init();
    });

    this.requestOtpForm = this.formBuilder.group({
      mobileNumber: new FormControl('', [Validators.required, Validators.pattern('^([+][9][1]|[9][1]|[0]){0,1}([4-9]{1})([0-9]{9})$')]),
      emailId: new FormControl('', [Validators.pattern('^([A-Za-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3})$'), Validators.maxLength(50)])
    });

    this.submitOtpForm = this.formBuilder.group({
      otp: new FormControl('', [Validators.required, Validators.maxLength(4), Validators.pattern('^[0-9]{4}$')])
    });
  }

  /* Init the page title and buttons */
  private init() {
    //this.globalservice.otpInfo(null);

    this.buttonTitle = "Request OTP";
    this.gotOTP = false;
    this.setTitle();
  }

  private setTitle() {
    switch (this.router.url) {
      case '/validate': this.title = 'You can reset your Password Here'
        this.no_email = false;
        break;
      default: break;
    }
  }

  requestOTP() {
    let data;
    this.gotOTP = true;
    this.buttonTitle = "Request OTP Again"
    this.globalservice.setLoadingFlag(true);
    switch (this.router.url) {
      case '/validate':
        data = this.requestOtpForm.value;
        this.validateService.requestCustomerOTP(AppSettings.COMMON_ENDPOINT + 'forgotPassword/verfiyMobileId', data)
          .subscribe(response => {
            //Do Nothing
          },
            err => {
              this.globalservice.setLoadingFlag(false);
            },
            () => {
              this.globalservice.setLoadingFlag(false);
            });
        break;
      default:
        this.globalservice.setLoadingFlag(false);
        break;
    }
  }

  submitOTP() {
    let data;
    if (this.submitOtpForm.valid) {
      data = this.submitOtpForm.value;
      data.mobileNumber = this.requestOtpForm.controls['mobileNumber'].value;
      switch (this.router.url) {
        case '/validate':
          this.validateService.submitCustomerOTP(AppSettings.COMMON_ENDPOINT + 'getSmsOtp', data)
            .subscribe(response => {
              this.globalservice.otpInfo(this.requestOtpForm.value);
              this.router.navigate(['/forgotPassword']);
            });
          break;
        default:
          break;
      }
    }

  }

  public hasError = (controlName: string, errorName: string) => {
    return this.requestOtpForm.controls[controlName].hasError(errorName);
  }

  public hasErrorOtp = (controlName: string, errorName: string) => {
    return this.submitOtpForm.controls[controlName].hasError(errorName);
  }

  ngOnDestroy() {
    this.globalservice.setLoadingFlag(false);
  }

}
