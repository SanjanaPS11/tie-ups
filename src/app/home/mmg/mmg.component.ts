import { Component, OnInit } from '@angular/core';
import { MmgService } from './mmg.service';

@Component({
  selector: 'app-mmg',
  templateUrl: './mmg.component.html',
  providers: [MmgService],
  styleUrls: ['./mmg.component.css']
})
export class MmgComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
