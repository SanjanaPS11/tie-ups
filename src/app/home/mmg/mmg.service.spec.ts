import { TestBed } from '@angular/core/testing';

import { MmgService } from './mmg.service';

describe('MmgService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MmgService = TestBed.get(MmgService);
    expect(service).toBeTruthy();
  });
});
