import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MmgComponent } from './mmg.component';

describe('MmgComponent', () => {
  let component: MmgComponent;
  let fixture: ComponentFixture<MmgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MmgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MmgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
