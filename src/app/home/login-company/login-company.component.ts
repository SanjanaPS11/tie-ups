import { Component, OnInit } from '@angular/core';
 
import { CompanyService } from '../company/company.service';
import { Router} from '@angular/router';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { AbstractControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-login-company',
  templateUrl: './login-company.component.html',
  styleUrls: ['./login-company.component.css']
})
export class LoginCompanyComponent implements OnInit {

loginForm:any;
  constructor(private companyService:CompanyService, private routes:Router,private formBuilder: FormBuilder) { 
     this.loginForm = this.formBuilder.group({
        userName: new FormControl('', [Validators.required]),
        Password: new FormControl(null, [Validators.required]),
       
  });
  }
  msg:any;
data:any;
res:any;
  ngOnInit() {
  }

login()
{
   this.data = this.loginForm.value;

  this.companyService.getLogin(this.data).subscribe(response=>{  
  this.res={...response};
  this.res=this.res.data;
    
        this.loginForm.reset();
      });
     if(this.res==true)
  {
    this.routes.navigate(['/company']);
  }
  else{
    this.msg='Invalid username or password';
  }
}
}

