import { Component, OnInit, HostListener } from '@angular/core';
import { Subscription } from 'rxjs';

import { AppSettings } from './constants/constants.module';
import { StorageService } from './service/storage.service';
import { Globalservice } from './service/globals.service';
import { AppService } from './app.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [AppService],
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'movemygoodspartner';
  screen: any;
  driverScreen: any;
  franchiseScreen: any;
  subscription: Subscription
  genders: { 'id': number; 'val': string; }[];
  country:any;
  state: any;
  tieupsScreen: number;
  operationScreen: number;

  constructor(private globalservice: Globalservice, private storageService: StorageService,
    private appService: AppService, private router: Router){}

  ngOnInit() {
    this.genders = [
      { 'id': 1, 'val': 'Male' },
      { 'id': 2, 'val': 'Female' }
    ];
    this.globalservice.genderList(this.genders);
    
    if (this.storageService.getItem('user') == null) {
      this.screen = null;
      this.globalservice.changeScreen(null);
    }
    else {
      this.screen = this.storageService.getItem('user').data.roleId;
      this.globalservice.changeScreen(this.storageService.getItem('user').data.roleId);
    }

  
    this.tieupsScreen = AppSettings.TIEUPS_ROLE_ID;
    this.operationScreen = AppSettings.OPERATIONAL_ROLE_ID;


    this.subscription = this.globalservice.navItem$
      .subscribe(item => {
        this.screen = item;
      });
        /* Get country state (only for India) */
    this.appService.getCountry(AppSettings.OWNER_ENDPOINT + 'country')
    .subscribe(response => {
      this.country = { ...response };
      this.globalservice.setCountry(this.country.data);
    });

  this.appService.getState(AppSettings.OWNER_ENDPOINT + 'State/country/' + AppSettings.INDIA)
    .subscribe(response => {
      this.state = { ...response };
      this.globalservice.setState(this.state.data);
    });
  }

  isShow: boolean;
  topPosToStartShowing = 100;

  @HostListener('window:scroll')
  checkScroll() {

    // window의 scroll top
    // Both window.pageYOffset and document.documentElement.scrollTop returns the same result in all the cases. window.pageYOffset is not supported below IE 9.

    const scrollPosition = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;

    if (scrollPosition >= this.topPosToStartShowing) {
      this.isShow = true;
    } else {
      this.isShow = false;
    }
  }

  // TODO: Cross browsing
  gotoTop() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }

  ngOnDestroy() {
    /* prevent memory leak when component is destroyed */
    this.subscription.unsubscribe();
  }

}
