import { TestBed } from '@angular/core/testing';

import { TieupsDashboardService } from './tieups-dashboard.service';

describe('TieupsDashboardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TieupsDashboardService = TestBed.get(TieupsDashboardService);
    expect(service).toBeTruthy();
  });
});
