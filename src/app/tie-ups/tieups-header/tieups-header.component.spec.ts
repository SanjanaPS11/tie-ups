import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TieupsHeaderComponent } from './tieups-header.component';

describe('TieupsHeaderComponent', () => {
  let component: TieupsHeaderComponent;
  let fixture: ComponentFixture<TieupsHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TieupsHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TieupsHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
