import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Router } from '@angular/router';
import { MatDialogConfig, MatDialog } from '@angular/material';

import { StorageService } from '../../service/storage.service';
import { PopupGeneralComponent } from '../../popup-general/popup-general.component';

@Component({
  selector: 'app-tieups-header',
  templateUrl: './tieups-header.component.html',
  styleUrls: ['./tieups-header.component.css']
})
export class TieupsHeaderComponent implements OnInit {
  slidesStore: any;
  selectedSlide: any;
  selectedItem: any;

  customOptions: OwlOptions  = {
    loop: false,
    margin: 0,
    nav: true,
    dots: false,
    autoplay: false,
    autoplayHoverPause: false,
    smartSpeed: 900,
    navText: ["<div class='owl-nav owl-prev'><i class='fa fa-chevron-left text-dark'></i></div>", "<div class='owl-nav owl-next'><i class='fa fa-chevron-right text-dark'></i></div>"],
    responsive: {
      0: {
        items: 3
      },
      600: {
        items: 3
      },
      1000: {
        items: 3
      }
    }
  };

  partnerSlides = [
    { 'id': 0, 'name': 'Franchise', 'url': 'franchiseView' },
    { 'id': 1, 'name': 'Fleet', 'url': 'fleetView' },
    { 'id': 2, 'name': 'Tie Ups', 'url': 'tieupsView' }
  ];

  fareFinanceSlides = [
    { 'id': 0, 'name': 'Finance & Forecast', 'url': 'financeForecastView' },
    { 'id': 1, 'name': 'Fare & Environments', 'url': 'fareView' }
  ];

  bottomNavItems = [
    { 'id': 0, 'name': 'Dashboard', 'class': 'fas fa-home', 'url': 'ownerDashboard', 'data': null },
    { 'id': 1, 'name': 'General', 'class': 'fas fa-user-cog', 'url': 'helpCenter', 'data': null },
    { 'id': 2, 'name': 'Partners', 'class': 'fas fa-handshake', 'url': null, 'data': this.partnerSlides },
    { 'id': 3, 'name': 'Fare & Finance', 'class': 'fas fa-chart-line', 'url': null, 'data': this.fareFinanceSlides },
    { 'id': 4, 'name': 'Logout', 'class': 'fas fa-power-off', 'url': null, 'data': null }
  ];

  constructor(private router: Router, private dialog: MatDialog, private storageService: StorageService) { }

  ngOnInit() {
    this.selectedItem = this.bottomNavItems[0].name;
    if (this.bottomNavItems[0].url) {
      this.router.navigate(['/',this.bottomNavItems[0].url]);
    }
    else if (this.bottomNavItems[0].data) {
      this.router.navigate(['/',this.bottomNavItems[0].data[0].url])
    }
  }

  changeCarousel(data) {
    this.selectedSlide = data.name;
    this.router.navigate([data.url]);
  }

  bottomNavChange(item) {
    if (!item)
      return;

    this.slidesStore = item.data;
    this.selectedItem = item.name;
    
    if (item.url) {
      this.router.navigate(['/',item.url]);
      return;
    }

    if (this.slidesStore) {
      this.selectedSlide = this.slidesStore[0].name;
      this.router.navigate([this.slidesStore[0].url]);
    }

    switch (this.selectedItem) {
      case 'Logout':
        this.logout();
        break;
      default:
        break;
    }
  }

  logout() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title : 'Are You Sure Want To Logout ?'
    }

    const dialogRef = this.dialog.open(PopupGeneralComponent,
      dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        if (data == undefined)
          return;
        //this.notificationService.deleteToken();
        this.storageService.clearStorage();
        this.router.navigate(['/']).then(() => {
          window.location.reload();
        });
    });
  }

}

