import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable()
export class AppSettings {
    
    constructor(){};

    public static COMMON_ENDPOINT = environment.production ? 'https://'+environment.commonUrl+':8083/mmg/api/v1/' : 'http://'+environment.commonUrl+':8083/mmg/api/v1/';
    public static WAREHOUSE_ENDPOINT = environment.production ? 'https://'+environment.warehouseUrl+':8084/mmg/api/v1/': 'http://'+environment.warehouseUrl+':8084/mmg/api/v1/';
    public static OWNER_ENDPOINT = environment.production ? 'https://'+environment.ownerUrl+':8085/mmg/api/v1/': 'http://'+environment.ownerUrl+':8085/mmg/api/v1/';
    public static FRANCHISE_ENDPOINT = environment.production ? 'https://'+environment.franchiseUrl+':8086/mmg/api/v1/': 'http://'+environment.franchiseUrl+':8086/mmg/api/v1/';
    public static CUSTOMER_ENDPOINT = environment.production ? 'https://'+environment.customerUrl+':8087/mmg/api/v1/': 'http://'+environment.customerUrl+':8087/mmg/api/v1/';
    public static TIEUP_ENDPOINT = environment.production ? 'https://'+environment.tieupUrl+':8088/mmg/api/v1/': 'http://'+environment.tieupUrl+':8088/mmg/api/v1/';
    public static FLEET_ENDPOINT = environment.production ? 'https://'+environment.fleetUrl+':8089/mmg/api/v1/': 'http://'+environment.fleetUrl+':8089/mmg/api/v1/';
    public static FARE_ENDPOINT = environment.production ? 'https://'+environment.fareUrl+':8090/mmg/api/v1/': 'http://'+environment.fareUrl+':8090/mmg/api/v1/';

    public static CUSTOMER_ROLE_ID          = 1;
    public static DRIVER_ROLE_ID            = 2;
    public static FLEET_ROLE_ID             = 3;
    public static WAREHOUSE_ROLE_ID         = 4;
    public static OWNER_ROLE_ID             = 5;
    public static TIEUPS_ROLE_ID            = 7;
    public static FRANCHISE_ROLE_ID         = 9;
    public static ADMIN_ROLE_ID             = 10;
    public static ENTERPRISE_ROLE_ID        = 19;
    public static OPERATIONAL_ROLE_ID       = 18;

    public static INDIA = 101;

    public static FIELD_ENGINEER_1 = 8073147507;
    public static FIELD_ENGINEER_2 = 9663741286;

    public static ERROR_MSGS = 
    {
        REQUIRED: 'Field is required!!',
        MAX_LENGTH: 'Max length Reached!!',
        MAX: 'Max Value Reached!!',
        EMAIL: 'Please Provide Proper Email!!',
        MOBILE: 'Please Provide Proper Mobile Number!!',
        PINCODE: 'Please Provide Proper Pincode!!',
        AADHAR: 'Please Provide Proper Aadhar Number!!',
        PAN: 'Please Provide Proper PAN Number!!',
        ACCOUNT_NO: 'Please Provide Proper Account Number!!',
        IFSC: 'Please Provide Proper IFSC Code!!',
        CONTRACT: 'Max Contract Time is Two years!!',
        REGISTRATION: 'Please Provide Proper Vehicle Registration Number',
        MIN_TWO: 'Min character Required is Two!!',
        MAX_FOUR: 'Max OTP Code is Four!!',
        PASSWORD: 'Minimum 8 characters are needed for Password!!',
        PASSWORD_PATTERN: 'Password should contain minimum of 8 characters and have atleast One UpperCase, LowerCase, Special Character and Number!!',
        OTP: 'OTP has to be digits, not Alphabets!!',
        MIN_WEIGHT: 'Min Weight Required is 40Kg!!',
        MAX_WEIGHT: 'Max Weight Reached!!',
        MIN_HEIGHT: 'Min Height Required is 100cm!!',
        MAX_HEIGHT: 'Max Height Reached!!',
        MISMATCH: 'Password Mismatch!!',
        USERNAME: 'Please Provide Proper Email or Mobile Number!!',
        EMISSION_TWO_YEARS: 'Emission is required for vehicles older than 2 years!!'
    };
 }