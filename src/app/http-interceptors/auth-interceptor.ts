import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from '../service/storage.service';
import { Globalservice } from '../service/globals.service';

import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';

//import { AuthService } from '../auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor( private storageService: StorageService, private globalservice: Globalservice,
    private router: Router) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    
    //const authReq = null;
    
    /* TODO */
    // Get the auth token from the service.
    //const authToken = this.auth.getAuthorizationToken();

    /*
    * The verbose way:
    // Clone the request and replace the original headers with
    // cloned headers, updated with the authorization.
    const authReq = req.clone({
      headers: req.headers.set('Authorization', authToken)
    });
    */
   
/*Clone the request and set the new header in one step.*/

/* Check for Expiry */

    if (this.storageService.getItem('user')!=null && this.storageService.getItem('user').data.tokenExpires) {
        let currentdate = new Date ();
        let expiredDate = new Date (this.storageService.getItem('user').data.tokenExpires);
        if (currentdate > expiredDate)
        {
          /* Log out if expires */
          this.globalservice.setLogoutFlag(true);
        }

    }


/* If the headers are already set. For example in case of image upload, Do not change the headers as it has to be
    multipart/form-data */    
    if(req.headers.get('X-API-KEY')) {
      return next.handle(req);
    }
    else {
      let authReq;
      if (this.storageService.getItem('user')!=null && this.storageService.getItem('user').data.accessToken)
      {
        authReq = req.clone({ setHeaders: { 'Content-Type':  'application/json',
          "X-API-KEY": "MMGATPL", "Authorization": 'Bearer '+ this.storageService.getItem('user').data.accessToken } });
      }
      else
      {
        authReq = req.clone({ setHeaders: { 'Access-control-allow-origin':'*', 'Content-Type':  'application/json', "X-API-KEY": "MMGATPL" } });
      }

/* send cloned request with header to the next handler. */
      return next.handle(authReq);
    }
  }
}
