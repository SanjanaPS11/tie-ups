import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';

import { Observable } from 'rxjs';

@Injectable()
export class EnsureHttpsInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

/* clone request and replace 'http://' with 'https://' at the same time */
    const secureReq = req.clone({
      url: environment.production ? req.url.replace('http://', 'https://') : req.url.replace('http://', 'http://')
    });

/* send the cloned, "secure" request to the next handler. */
    //console.log(secureReq);
    return next.handle(secureReq);
  }
}
