import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupGeneralComponent } from './popup-general.component';

describe('PopupGeneralComponent', () => {
  let component: PopupGeneralComponent;
  let fixture: ComponentFixture<PopupGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
