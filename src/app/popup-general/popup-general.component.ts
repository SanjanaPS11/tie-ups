import { Component, OnInit, Inject } from '@angular/core';

import { MAT_DIALOG_DATA, MatDialogRef, MatDialog, MatDialogConfig } from "@angular/material";
import { CompanyService } from '../home/company/company.service';
@Component({
  selector: 'app-popup-general',
  templateUrl: './popup-general.component.html',
  styleUrls: ['./popup-general.component.css']
})
export class PopupGeneralComponent implements OnInit {

  public title: String;
  public Button: String;
public id:String;
  constructor(private dialogRef: MatDialogRef<PopupGeneralComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: any) { }

  ngOnInit() {
    if(this.dialogData)
    {
      this.title = this.dialogData.title;
      this.Button=this.dialogData.name;
      this.id=this.dialogData.uuId;
      console.log("fgr")
    }
    else
      this.dialogRef.close();
  }

  close()
  {
    this.dialogRef.close();
  }

  save()
  {
   
    this.dialogRef.close(this.title);
  }
}
