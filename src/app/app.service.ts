import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { HttpErrorHandler, HandleError } from './http-error-handler.service';

import { Observable } from 'rxjs';
import { catchError, map, retry, timeout } from 'rxjs/operators';

@Injectable()
export class AppService {
  private handleError: HandleError;
  
  constructor(
    private http: HttpClient,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('AppService');
  }

  getCountry(url) {
    return this.http.get(url)
    .pipe(
      map(this.extractData),
      catchError(this.handleError('getCountry', []))
    );
  }

  getState(url) {
    return this.http.get(url)
    .pipe(
      map(this.extractData),
      catchError(this.handleError('getState', []))
    );
  }

  getCity(url) {
    return this.http.get(url)
    .pipe(
      map(this.extractData),
      catchError(this.handleError('getCity', []))
    );
  }

  getGoodsType(url) {
    return this.http.get(url)
    .pipe(
      map(this.extractData),
      catchError(this.handleError('getGoodsType', []))
    );
  }

  getWeights(url) {
    return this.http.get(url)
    .pipe(
      map(this.extractData),
      catchError(this.handleError('getWeights', []))
    );
  }
  
  private extractData(res: Response) {
    let body = res;
    return body || { };
  }
}
