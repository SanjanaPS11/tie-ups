export const environment = {
  production: true,
  version: 'jenkinsBuildNO',
  hostUrl: 'https://www.movemygoods.in',
  commonUrl: 'www.movemygoods.in',
  warehouseUrl: 'www.movemygoods.in',
  ownerUrl: 'www.movemygoods.in',
  franchiseUrl: 'www.movemygoods.in',
  customerUrl: 'www.movemygoods.in',
  tieupUrl: 'www.movemygoods.in',
  fleetUrl: 'www.movemygoods.in',
  fareUrl: 'www.movemygoods.in',
  paymentUrl: 'https://secure.payu.in/_payment',
  firebase: {
    apiKey: "AIzaSyD6U_rXc-W2vXPln4kb5_m5K-XndmaRoGU",
    authDomain: "mmgclient-fff6d.firebaseapp.com",
    databaseURL: "https://mmgclient-fff6d.firebaseio.com",
    projectId: "mmgclient-fff6d",
    storageBucket: "mmgclient-fff6d.appspot.com",
    messagingSenderId: "936410373912",
    appId: "1:936410373912:web:c5bc061a8e93eb26"
  }
};
