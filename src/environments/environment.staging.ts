export const environment = {
  production: false,
  version: 'jenkinsBuildNO',
  hostUrl: 'http://35.154.73.99',
  commonUrl: '13.235.70.80',
  warehouseUrl: '52.66.176.189',
  ownerUrl: '13.233.118.81',
  franchiseUrl: '52.66.255.132',
  customerUrl: '13.235.80.98',
  tieupUrl: '52.66.83.82',
  fleetUrl: '52.66.176.189',
  fareUrl: '13.235.79.87',
  paymentUrl: 'https://sandboxsecure.payu.in/_payment',
  firebase: {
    apiKey: "AIzaSyD6U_rXc-W2vXPln4kb5_m5K-XndmaRoGU",
    authDomain: "mmgclient-fff6d.firebaseapp.com",
    databaseURL: "https://mmgclient-fff6d.firebaseio.com",
    projectId: "mmgclient-fff6d",
    storageBucket: "mmgclient-fff6d.appspot.com",
    messagingSenderId: "936410373912",
    appId: "1:936410373912:web:c5bc061a8e93eb26"
  }
};
