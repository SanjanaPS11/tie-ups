// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  version: 'jenkinsBuildNO',
  hostUrl: 'http://35.154.73.99',
  commonUrl: '3.6.208.144',
  warehouseUrl: '52.66.176.189',
  ownerUrl: '13.126.134.220',
  franchiseUrl: '52.66.255.132',
  customerUrl: '13.235.80.98',
  tieupUrl: '52.66.83.82',
  fleetUrl: '52.66.176.189',
  fareUrl: '13.235.79.87',

  // commonUrl: '15.207.198.80',
  // ownerUrl: '13.126.197.2',
  // franchiseUrl: '13.235.20.30',
  // customerUrl: '13.126.197.2',


  // warehouseUrl: '52.66.176.189',
  // tieupUrl: '52.66.83.82',
  // fleetUrl: '52.66.176.189',
  // fareUrl: '13.235.79.87',
  paymentUrl: 'https://sandboxsecure.payu.in/_payment',
  firebase: {
    apiKey: "AIzaSyD6U_rXc-W2vXPln4kb5_m5K-XndmaRoGU",
    authDomain: "mmgclient-fff6d.firebaseapp.com",
    databaseURL: "https://mmgclient-fff6d.firebaseio.com",
    projectId: "mmgclient-fff6d",
    storageBucket: "mmgclient-fff6d.appspot.com",
    messagingSenderId: "936410373912",
    appId: "1:936410373912:web:c5bc061a8e93eb26"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
