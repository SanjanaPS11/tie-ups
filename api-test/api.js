var supertest = require('supertest'),
    URL = {}, PRODUCTION = 'production', STAGING = 'staging', LOCALOST = 'localhost';

module.exports = {
    init_api_url: function (build_type) {
        URL = {
            COMPANY_ENDPOINT   : (build_type == PRODUCTION) ? supertest('https://movemygoods:8085/mmg/api/')
                               : ((build_type == STAGING) ? supertest('http://13.233.153.114:8085/mmg/api/')
                               : supertest('http://localhost:8085/demo/api/')),

            COMMON_ENDPOINT    : (build_type == PRODUCTION) ? supertest('https://movemygoods:8083/mmg/api/')
                               : ((build_type == STAGING) ? supertest('http://3.6.208.144:8083/mmg/api/')
                               : supertest('http://localhost:8083/mmg/api/')),
            WAREHOUSE_ENDPOINT : (build_type == PRODUCTION) ? supertest('https://movemygoods:8084/mmg/api/')
                               : ((build_type == STAGING) ? supertest('http://52.66.176.189:8084/mmg/api/')
                               : supertest('http://localhost:8084/mmg/api/')),
            OWNER_ENDPOINT     : (build_type == PRODUCTION) ? supertest('https://movemygoods:8085/mmg/api/')
                               : ((build_type == STAGING) ? supertest('http://13.126.197.2:8085/mmg/api/')
                               : supertest('http://localhost:8085/mmg/api/')),
            FRANCHISE_ENDPOINT : (build_type == PRODUCTION) ? supertest('https://movemygoods:8085/mmg/api/')
                               : ((build_type == STAGING) ? supertest('http://13.235.20.30:8085/mmg/api/')
                               : supertest('http://localhost:8085/mmg/api/')),
            CUSTOMER_ENDPOINT  : (build_type == PRODUCTION) ? supertest('https://movemygoods:8085/mmg/api/')
                               : ((build_type == STAGING) ? supertest('http://13.126.197.2:8085/mmg/api/')
                               : supertest('http://localhost:8085/mmg/api/')),
            TIEUP_ENDPOINT     : (build_type == PRODUCTION) ? supertest('https://movemygoods:8085/mmg/api/')
                               : ((build_type == STAGING) ? supertest('http://13.235.20.30:8085/mmg/api/')
                               : supertest('http://localhost:8085/mmg/api/')),
            FLEET_ENDPOINT     : (build_type == PRODUCTION) ? supertest('https://movemygoods:8085/mmg/api/')
                               : ((build_type == STAGING) ? supertest('http://3.7.55.71:8085/mmg/api/')
                               : supertest('http://localhost:8085/mmg/api/')),
            FARE_ENDPOINT      : (build_type == PRODUCTION) ? supertest('https://movemygoods:8085/mmg/api/')
                               : ((build_type == STAGING) ? supertest('http://13.233.153.114:8085/mmg/api/')
                               : supertest('http://localhost:8085/mmg/api/'))
        }
    },
    get_endpoint_url : function () {
        return URL;
    }
};
