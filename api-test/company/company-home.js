/* env BUILD=production mocha test.js */
var build_type = process.env.BUILD,
    api = require('../api.js'),
    userDetails = undefined,
    url_endpoint = {},
    json_data = {};
    user_data = {};
    should = require('chai').should(),
    expect = require('chai').expect;

/* JSON File Read */
const fs = require('fs');

fs.readFile('./modal/company.json', 'utf8', (err, jsonString) => {
    if (err) {
        console.log("File read failed:", err)
        return
    }
    company_data = JSON.parse(jsonString);
})

/* Get list of all countries */
getCompany = function () {
    it('Get Country List API', function (done) {
        url_endpoint.COMPANY_ENDPOINT.get('v1/company')
            .set('accept', 'application/json')
            .set('x-api-key', 'MMGATPL')
            .expect(200)
            .end(function (err, res) {
                console.log(err);
                expect(res.statusCode).to.equal(200); 
                expect(res.body.data.list).to.be.an('array'); 
                expect(res.body.status).to.equal('SUCCESS'); 
                done();
            });
    });
}

/* API Test for Company Registraion */
company_Registration = function() {
    it('Company Registration', function (done) {
        url_endpoint.COMPANY_ENDPOINT.post('v1/company')
            .set('accept', 'application/json')
            .set('x-api-key', 'MMGATPL')
            .send(company_data.company_set1)
            .expect(200)
            .end(function (err, res) {
                console.log(res.body);
                done();
            });
    });
}

start = function () {
    url_endpoint = api.get_endpoint_url();
    if (!url_endpoint)
        return;
    getCompany();
    company_Registration();
  
}

init = function () {
    console.log("--------START COMPANY HOME API TESTING!!!!--------")
    api.init_api_url(build_type);
    start();
}

init();

module.exports = {
    init: function() {
        this.init();
    },
    getUserData: function() {
        return userDetails;
    },
    getUrl: function() {
        return url_endpoint;
    }
}