/* env BUILD=production mocha test.js */
var build_type = process.env.BUILD,
    api = require('../api.js'),
    userDetails = undefined,
    url_endpoint = {},
    json_data = {};
    user_data = {};
    should = require('chai').should(),
    expect = require('chai').expect;

/* JSON File Read */
const fs = require('fs');

fs.readFile('./modal/franchise.json', 'utf8', (err, jsonString) => {
    if (err) {
        console.log("File read failed:", err)
        return
    }
    json_data = JSON.parse(jsonString);
})

fs.readFile('./modal/user.json', 'utf8', (err, jsonString) => {
    if (err) {
        console.log("File read failed:", err)
        return
    }
    user_data = JSON.parse(jsonString);
})

/* Get list of all countries */
getCountry = function () {
    it('Get Country List API', function (done) {
        url_endpoint.OWNER_ENDPOINT.get('v1/country')
            .set('accept', 'application/json')
            .set('x-api-key', 'MMGATPL')
            .expect(200)
            .end(function (err, res) {
                console.log(err);
                expect(res.statusCode).to.equal(200); 
                expect(res.body.data.list).to.be.an('array'); 
                expect(res.body.status).to.equal('SUCCESS'); 
                done();
            });
    });
}

/* Get list of all States for INDIA */
getState = function () {
    it('Get State List API for INDIA', function (done) {
        url_endpoint.OWNER_ENDPOINT.get('v1/State/country/101')
            .set('accept', 'application/json')
            .set('x-api-key', 'MMGATPL')
            .expect(200)
            .end(function (err, res) {
                //console.log(err);
                expect(res.statusCode).to.equal(200); 
                expect(res.body.data.list).to.be.an('array'); 
                expect(res.body.status).to.equal('SUCCESS'); 
                done();
            });
    });
}

/* Franchise Enquiry Request */
franchiseEnquiryRequest = function () {
    it('Send Franchise Request', function (done) {
        url_endpoint.COMMON_ENDPOINT.post('v2/boarding/request')
            .set('accept', 'application/json')
            .set('x-api-key', 'MMGATPL')
            .send(json_data.franchise_enquiry_set1)
            .expect(200)
            .end(function (err, res) {
                console.log(res.body.message);
                done();
            });
    });
}

/* User Login */
login = function() {
    it('Login Customer', function (done) {
        url_endpoint.COMMON_ENDPOINT.post('v2/login')
            .set('accept', 'application/json')
            .set('x-api-key', 'MMGATPL')
            .send({password: "Welcome!1", roleId: 1, userName: "9611203049"})
            .expect(200)
            .end(function (err, res) {
                if(res.body.status == 'SUCCESS')
                {
                    userDetails = res.body.data;
                }
                else{
                    console.log(res.body.message);    
                }
                done();
            });
    });
}

/* API Test for User Registraion */
user_registration = function() {
    it('User Registration', function (done) {
        url_endpoint.COMMON_ENDPOINT.post('v2/profile')
            .set('accept', 'application/json')
            .set('x-api-key', 'MMGATPL')
            .send(user_data.user_set1)
            .expect(200)
            .end(function (err, res) {
                console.log(res.body);
                done();
            });
    });
}

start = function () {
    url_endpoint = api.get_endpoint_url();
    if (!url_endpoint)
        return;
    getCountry();
    getState();
    franchiseEnquiryRequest();
    user_registration();
    login();
}

init = function () {
    console.log("--------START CUSTOMER HOME API TESTING!!!!--------")
    api.init_api_url(build_type);
    start();
}

init();

module.exports = {
    init: function() {
        this.init();
    },
    getUserData: function() {
        return userDetails;
    },
    getUrl: function() {
        return url_endpoint;
    }
}