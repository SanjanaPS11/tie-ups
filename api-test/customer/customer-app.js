var customer_home = require('./customer-home.js'),
    userDetails = undefined,
    url_endpoint = {},
    should = require('chai').should(),
    expect = require('chai').expect;

getUserProfile = function () {
    it('Get User profile', function (done) {
        userDetails = customer_home.getUserData();
        url_endpoint = customer_home.getUrl();
        url_endpoint.COMMON_ENDPOINT.get('v2/profile/' + userDetails.id + '?roleId=' + userDetails.roleId)
            .set('accept', 'application/json')
            .set('x-api-key', 'MMGATPL')
            .set('Authorization', 'Bearer ' + userDetails.accessToken)
            .expect(200)
            .end(function (err, res) {
                expect(res.statusCode).to.equal(200); 
                expect(res.body.status).to.equal('SUCCESS'); 
                done();
            });
    });
}

init = function() {
    setTimeout(function() {
        //Do Nothing
    }, 5000);
    getUserProfile();
}

init();